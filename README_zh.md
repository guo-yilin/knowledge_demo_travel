# knowledge_demo_travel

## 简介
基于智慧出行场景围绕出行打造的样例，开发者可以从多种类型设备间协同联动特性中感受万物互联带来的智能。

![](./media/travel_view.png)

## 样例汇总
### 连接模组类应用
+ [智能停车场_车辆出入检测器](docs/GreyWolf_CarApproachDetect/README.md)
+ [智能停车场_门闸控制器](docs/GreyWolf_Latch/README.md)

### 带屏IoT设备应用



### Camera应用

- [智能停车场_车牌识别器（云识别版本）](docs/GreyWolf_ImageRecognition_RemoteAI/README.md)
- [智能停车场_车牌识别器（OpenCV版本）](docs/GreyWolf_ImageRecognition_LocalAI/README.md)
### 标准系统应用
- [智能停车场_车辆管理中控台](docs/GreyWolf_ParkingManageSys_FA/README.md)
- [旅行小助手_AI智能旅行方案规划](FA/AITravelAssistant)
- [智能门禁人脸识别(eTS)](docs/FaceRecognition_ETS/README_zh.md)
- [智能门禁人脸识别(JS)](docs/FaceRecognition_JS/README_zh.md)
- [天气预报(eTS)坚果造](docs/WeatherForeCast/README_zh.md)
- [智联汽车(eTS)坚果造](docs/SmartCenterCar/README_zh.md)
- [OHCar模拟车机](docs/OpenHarmonyCarSystem_OHCar/README.md)
- [博泰OpenHarmony语音助理](docs/PATEO_CarVoiceAssistant)

## 参考资料

+ [OpenHarmony官网](https://www.openharmony.cn/)
+ [OpenHarmony知识体系仓库](https://gitee.com/openharmony-sig/knowledge)
+ [OpenHarmony知识体系议题申报](https://docs.qq.com/sheet/DUUNpcWR6alZkUmFO)
+ [OpenHarmony知识体系例会纪要](https://gitee.com/openharmony-sig/sig-content/tree/master/knowlege/meetings)
+ [OpenHarmony开发样例共建说明](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/co-construct_demos/README_zh.md)
+ [OpenHarmony知识体系-智能家居场景](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
+ [OpenHarmony知识体系-影音娱乐场景](https://gitee.com/openharmony-sig/knowledge_demo_entainment)
+ [OpenHarmony知识体系-购物消费场景](https://gitee.com/openharmony-sig/knowledge_demo_shopping)

