[旅行小助手](../../FA/AITravelAssistant/项目源码)
=========
**项目介绍**<br>
>该项目根据游玩目的地，综合用户特点、游玩时长、景点分布等多重因素，为用户推荐合适的旅游方案。<br>
>该项目前端采用deveco studio编写，后端基于django框架编写，服务器选取华为云服务器。
>推荐模型的构建采用神经网络模型。景点信息主要来源于去哪儿网。
>景点间的路线距离及耗费时间信息通过高德的api获取。<br>
>相较于当下流行的旅游攻略分享，我们减轻了用户自行搜寻整合攻略等耗时耗力的工作负担，为用户带来了更加轻松的旅行体验。<br>
>当下主流的旅行app中，关于旅行路线的规划，主要为用户分享旅游攻略和花钱定制旅行方案。
>仅携程旅行提供自动生成旅行路线规划的功能，但搜索自由度不高（仅能在有限关键词进行检索），
>且存在漏掉必玩景点、重复游玩相似景点等问题。我们将针对这些问题进一步优化算法，规划出更优质的方案。


# 软件架构

软件架构说明
![框架图](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/softframe.png)

# 界面展示

## 1、登录、注册页面

<center><img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/login.png"  style="zoom:40%;" />                                                      <img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/register.png"  style="zoom:40%;" /></center>

## 2、城市选择页面

<center><img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/home.png"  style="zoom:40%;" />                                                      <img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/home_pick.png"  style="zoom:40%;" /></center>

## 3、景点推荐及挑选页面

<center><img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/choose.png"  style="zoom:40%;" /></center>

## 4、景点介绍界面

<center><img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/detail1.png"  style="zoom:40%;" />                                                      <img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/detail2.png"  style="zoom:40%;" /></center>

## 5、方案推荐界面

<center><img src="https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/show.png"  style="zoom:40%;" /></center>


# 开发文档：

## 1、爬虫部分[【爬虫】](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E9%A1%B9%E7%9B%AE%E8%AF%A6%E7%BB%86%E8%A7%A3%E8%AF%BB/%E7%88%AC%E8%99%AB.md)

## 2、推荐模型部分[【推荐模型框架】](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E9%A1%B9%E7%9B%AE%E8%AF%A6%E7%BB%86%E8%A7%A3%E8%AF%BB/%E6%8E%A8%E8%8D%90%E6%A8%A1%E5%9E%8B%E6%A1%86%E6%9E%B6.md)

## 3、前端设计部分[【前端设计】](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E9%A1%B9%E7%9B%AE%E8%AF%A6%E7%BB%86%E8%A7%A3%E8%AF%BB/%E5%89%8D%E7%AB%AF%E8%AE%BE%E8%AE%A1.md)

## 4、前后端连接部分[【前后端连接】](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E9%A1%B9%E7%9B%AE%E8%AF%A6%E7%BB%86%E8%A7%A3%E8%AF%BB/%E5%89%8D%E5%90%8E%E7%AB%AF%E8%BF%9E%E6%8E%A5.md)

# 演示：
## [【视频链接】](https://www.bilibili.com/video/BV1zu411D7ED?spm_id_from=333.999.0.0)


# 不足与展望
参与本次项目的五位同学都缺乏完成大型项目的经验，在项目进行的过程中也走过很多的弯路，
不知道从哪里下手。此外，项目本身应建立于大型数据之上，但我们能够获取到的数据有限，
并且缺乏对数据的整合处理、模型构建的能力，导致项目demo的推荐存在有较大的偏差，
同时，因为后端云服务器的配置较差，后端的整个运算过程较慢，经常出现服务器断联的现象，
阻碍了项目的进展，但经过我们不断优化改善，项目雏形逐渐展现，变成了现在的模样。<br>
通过这次项目，我们更深入的了解到一些关于软件开发的整体框架与流程，同时也学习了很多课堂上学习不到的东西。此外，团队合作方面也有很大的进步。我们的项目可以说从头到尾没有一步是顺利的，但这并没有阻挡我们前进的步伐，我们也相信，经历过这次漫长曲折的学习实践之后，我们会更加不惧困难勇于挑战，也希望通过我们的努力，未来能促进行业甚至是社会的发展。



