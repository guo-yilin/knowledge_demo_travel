# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 21:48:37 2022

@author: Administrator
"""
import json
import requests
import itertools
import pandas as pd
import numpy as np

def route(ids):  
    du = []   #时间
    pos = []  #经纬度
    path = [] #路线
    cet = {
        "方案一":{
            "route":[],
            "duration":[],
            "distance":[]
            },
        "方案二":{
            "route":[],
            "duration":[],
            "distance":[]
            }
        }
    
    '''
    测试数据
    '''
    # ids = [1,2,3,4]
    # pos.append('116.481349,39.981214')
    # pos.append('116.481028,39.989643')
    # pos.append('116.481425,39.969404')
    # pos.append('116.492504,39.960356')

    # 读取景点表
    scenary_title = ['sid','sname', 'area','picture','jianjie', 'jinweidu']
    scenary = pd.read_table('D:\\tmp\\goodluck\\goodluck\\luck\\fff.txt', sep=':', header=None, names=scenary_title, engine = 'python')
    scenary = scenary.filter(regex='sname|jinweidu')

    # 将选取景点的经纬度加到pos数组中
    for i in ids:
        i = eval(i)
        ind = scenary.loc[scenary['sname'] == i]
        ind = ind.index[0]
        aaa = scenary.at[ind,"jinweidu"]
        pos.append(aaa)
        
    print(pos) 
    # 对景点进行全排列 对每一种方案进行时间的计算
    for idm in itertools.permutations(ids):
        z = ids.index(idm[0])
        y = ids.index(idm[-1])
        ori = pos[z]
        des = pos[y]
        mid = []
        idm = list(idm)
        for i in range(1,len(idm)-1):
            xy = ids.index(idm[i])
            mid.append(pos[xy])
        middle = ';'.join(str(i) for i in mid)
        
        # 连接高德 进行多点路线规划 得到该路线的长度及时间
        url = 'https://restapi.amap.com/v3/direction/driving'
        params = {
    
            'origin' : ori,
            'destination' : des,
            'waypoints' : middle,
            'extensions' : 'base',
            'strategy' : '10',
            'key' : '0f5f3d3534cb898ff44cfce104bcd59c',
            }
        res = requests.get(url,params)
        
            
        jd = json.loads(res.text)
        a = jd['route']
        b = a['paths']
        # print(b)
        c = b[0]['duration']   
        path.append(idm)
        du.append(int(c))
   
    mint=np.argsort(du)
    
    # 选取耗时最短的两条路线
    cet["方案一"]["route"] = path[mint[-1]]
    cet["方案二"]["route"] = path[mint[-3]]
   
    # 获取两条路线的具体信息
    for item in cet:      
        for i in range(0,len(cet[item]["route"])-1):
            za = cet[item]["route"][i]
            za = ids.index(za)
            ori = pos[za]
            zb = cet[item]["route"][i+1]
            zb = ids.index(zb)
            des = pos[zb]
         
         
            url = 'https://restapi.amap.com/v3/direction/driving'
            params = {        
                'origin' : ori,
                'destination' : des,
                'extensions' : 'base',
                'strategy' : '10',
                'key' : '0f5f3d3534cb898ff44cfce104bcd59c',
                }
            res = requests.get(url,params)
            
                
            jd = json.loads(res.text)
            a = jd['route']
            b = a['paths']
            c = int(b[0]['duration']) 
            g = c//3600
            h = (c%3600)//60
            if g < 1:
                d = "两地驾车时间约为" + str(h) + "min" 
            else:
                d = "两地驾车时间约为" + str(g) + "h" +str(h) + "min"        
            e = int(b[0]['distance']) 
            k = round(e/1000,2)
            l = e%1000
            if k < 1:
                
                f = "两地相距" + str(l) + 'm'
            else:
                f = "两地相距" + str(k) + 'km'      
            # f = "两地相距" + str(e) + 'm'      
            cet[item]["duration"].append(d)
            cet[item]["distance"].append(f)    
            
            
    return cet