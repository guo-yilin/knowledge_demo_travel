import router from '@system.router';
import fetch from '@system.fetch';
import prompt from '@system.prompt';

export default {
    data: {
        list1: [],
        list2:[],
        info: {},
        list:[],
        but:"显示方案",
    },
    ////////////页面初始化，跳转至该页面时自动触发////////////
    onInit() {
    },
    onReady(){
    },
    ////////////点击按钮时触发////////////
    onclick: function(e){
        //当按钮为显示方案时，将选择的所有景点名返回至后端，根据景点间的距离以及需要时间进行方案创建
        if (this.but == "显示方案")
        {
            var info = "";
            fetch.fetch({
                url: 'http://122.9.129.166:1918/show/',
                method: "POST",
                data: {
                    select : this.select,
                },
                success: (resp) => {
                    //令获取到的数据赋给info
                    info = resp.data;
                    info = JSON.parse(info)
                    prompt.showToast({
                        message: "数据加载中...",
                        duration: 3000
                    });
                    //console.log("info" + info)
                    //提取方案信息
                    for (var key of Object.keys(info)) {
                        //创建方案项目
                        var item = {
                            index: key,
                            route: info[key]["route"],
                            duration: info[key]["duration"],
                            distance: info[key]["distance"],
                            uri: "pages/detail/detail",
                        }
                        this.list.push(item)
                        //console.log("我是index"+ item.index);
                        //console.log("我是route"+ item.route);
                        //console.log("我是duration"+ item.duration);
                        //console.log("我是distance"+ item.distance);

                    }
                    //按钮切换为返回主页面
                    this.but = "返回主页面"
                },
                //数据传输失败时消息提示
                fail: (resp) => {
                    prompt.showToast({
                        message: "获取数据失败",
                        duration: 3000
                    });
                    console.log("获取数据失败")
                }
            });
        }
        //按钮为返回主页面时，返回主页面
        else{
            router.push({
            uri: "pages/home/home",
            params:{
                occupation: this.occupation,
                age: this.age,
                gender: this.gender,
                region: this.region,
                genre: this.genre,
            }
        });
        }

    },

    ////////////点击具体景点，跳转至景点信息页面////////////
    jumpPage(id, uri) {
        router.push({
            uri: uri,
            params: {
                interest: this.kaka[id],
            }//params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
        });
    }
}
