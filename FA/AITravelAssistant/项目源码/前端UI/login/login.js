import fetch from '@system.fetch';
import prompt from '@system.prompt';
import router from '@system.router';

export default {
    data: {
    },
    ////////////页面初始化，跳转至该页面时自动触发////////////
    onInit(){
    },
    ////////////账号输入框////////////
    inputuid(e) {
        this.uid = e.value;
    },
    ////////////密码输入框////////////
    inputPwd(e) {
        this.pwd = e.value;
    },

    ////////////点击登录按钮，将所输入账号密码返回至后端进行比对////////////
    login() {
        var info = "";
        fetch.fetch({
            url: 'http://122.9.129.166:1918/login/',
            method: "POST",
            data: {
                uid: this.uid,
                pwd: this.pwd,
            },
            //成功获取信息时
            success: (resp) => {
                 info = JSON.parse(resp.data);
                 //提示消息：登录成功或信息不正确
                 prompt.showToast({
                    message: info["i"],
                    duration: 3000
                });
                //console.log("data"+info["i"])
                //确认信息无误时将个人喜好信息传给主页面
                if(info["i"] == "登录成功"){
                    router.push({
                        uri: "pages/home/home",
                        params: {
                            occupation: info["user"]["occupation"],
                            age: info["user"]["age"],
                            gender: info["user"]["gender"],
                            region: info["user"]["region"],
                            genre: info["user"]["genre"],

                        }//params传输json格式数据给下一个界面，其他数据都会随着该界面的销毁而消失
                    });
                    //console.log(info["user"])
                }
            },
            //信息获取失败时
            fail: (resp) => {
                prompt.showToast({
                    message: "获取数据失败",
                    duration: 3000
                });
                console.log("获取数据失败")
            }
         });
    },
    ////////////点击注册按钮，跳转至注册页面////////////
    Jumppages(){
        router.push({
            uri: "pages/register/register",
        });
    }
}
