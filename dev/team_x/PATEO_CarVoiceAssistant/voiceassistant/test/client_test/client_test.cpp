/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "voice_assistant_client_manager.h"
#include "common_utils.h"
#include <ctime>
#include <iostream>
#include <nlohmann/json.hpp>

using namespace OHOS;
using namespace OHOS::CarVoiceAssistant;
using namespace std;

int main()
{
    VoiceAssistantClientManager* manager = VoiceAssistantClientManager::GetInstance();

    bool isEnableWakeUp = true;
    manager->IsEnableWakeUp(isEnableWakeUp);
    cout << "isEnableWakeUp:" << isEnableWakeUp << endl;

    while (true) {
        cout << "**************************************" << endl;
        cout << "1. change speaker" << endl;
        cout << "2. speak tts" << endl;
        cout << "3. start recognizer" << endl;
        cout << "4. stop recognizer" << endl;
        cout << "5. register hotwords" << endl;
        cout << "6. enable wakeup" << endl;
        cout << "7. disable wakeup" << endl;
        cout << "8. stop speak tts" << endl;
        cout << "**************************************" << endl;

        std::string line;
        getline(cin, line);
        if (line == "1") {
            cout << "input speaker:" << endl;
            std::string speaker;
            getline(cin, speaker);
            if (speaker.length() > 0) {
                manager->ChangeSpeakerType(speaker);
            }
        } else if (line == "2") {
            std::string tts = "大连市今天多云，气温15~21℃，西北风6~7级， 温度适宜。";
            CommonUtils::VoiceAssistantErrorCode result = CommonUtils::VOICE_ASSISTANT_OK;
            manager->PlayTTS(result, tts);
        } else if (line == "3") {
            CommonUtils::VoiceAssistantErrorCode result = CommonUtils::VOICE_ASSISTANT_OK;
            manager->StartRecognize(result);
            cout << "result:" << result << endl;
        } else if (line == "4") {
            manager->StopRecognize();
        } else if (line == "5") {
            std::list<std::map<std::string, std::string>> items;
            items.push_back({ { "title", "打开车窗" }, { "url", "uicontrol_common$Open_Car_Window" } });
            items.push_back({ { "title", "关闭车窗" }, { "url", "uicontrol_common$Close_Car_Window" } });
            items.push_back({ { "title", "打开空调" }, { "url", "uicontrol_common$Open_Car_Air" } });
            items.push_back({ { "title", "关闭空调" }, { "url", "uicontrol_common$Close_Car_Air" } });

            nlohmann::json j = items;
            std::string jsonStr = j.dump();

            manager->RegisterHotwords(jsonStr);
        } else if (line == "6") {
            manager->EnableWakeUp();
        } else if (line == "7") {
            manager->DisableWakeUp();
        } else if (line == "8") {
            manager->StopPlayTTS();
        }
    }

    return 0;
}