/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_COMMON_UTILS_H
#define CAR_VOICE_ASSISTANT_COMMON_UTILS_H

#include <ctime>
#include <openssl/sha.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <vector>

#define CAR_VOICE_ASSISTANT_SERVICE_SA_ID 5102

namespace OHOS::CarVoiceAssistant::CommonUtils {
enum VoiceAssistantErrorCode {
    VOICE_ASSISTANT_OK = 0,
    VOICE_ASSISTANT_ERR = 8001,
    VOICE_ASSISTANT_START_RECORD_FAILED, //录音开启失败
    VOICE_ASSISTANT_START_WEBSOCKET_CONNECT_FAILED, // websocket连接失败
};

} // namespace OHOS::CarVoiceAssistant::CommonUtils

#endif /* CAR_VOICE_ASSISTANT_COMMON_UTILS_H */
