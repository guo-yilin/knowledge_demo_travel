/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_LOG
#define CAR_VOICE_ASSISTANT_LOG

#include <cstring>
#include <string>

#include "hilog/log.h"

#define MAKE_FILE_NAME (strrchr(__FILE__, '/') + 1)

#define VOICE_ASSISTANT_LOG_TAG "CarVoiceAssistant"

#define VOICE_ASSISTANT_LOG_DOMAIN 0xD001C00

static constexpr OHOS::HiviewDFX::HiLogLabel NETSTACK_LOG_LABEL = {LOG_CORE, VOICE_ASSISTANT_LOG_DOMAIN, VOICE_ASSISTANT_LOG_TAG};

#define VOICE_ASSISTANT_HILOG_PRINT(Level, fmt, ...)                                                                        \
    (void)OHOS::HiviewDFX::HiLog::Level(NETSTACK_LOG_LABEL, "CarVoiceAssistant [%{public}s %{public}d] " fmt, MAKE_FILE_NAME, \
                                        __LINE__, ##__VA_ARGS__)


#define VOICE_ASSISTANT_LOGE(fmt, ...) VOICE_ASSISTANT_HILOG_PRINT(Error, fmt, ##__VA_ARGS__)

#define VOICE_ASSISTANT_LOGI(fmt, ...) VOICE_ASSISTANT_HILOG_PRINT(Info, fmt, ##__VA_ARGS__)

#endif /* CAR_VOICE_ASSISTANT_COMMON_UTILS_H */