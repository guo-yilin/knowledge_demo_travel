/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_WAKEUP_MANAGER_H
#define CAR_VOICE_ASSISTANT_WAKEUP_MANAGER_H

#include "i_wakeup_manager.h"
#include "refbase.h"
#include <mutex>
#include <pocketsphinx.h>
#include <webrtc_vad.h>

namespace OHOS {
namespace CarVoiceAssistant {

    class WakeUpManager : public IWakeUpManager {
    public:
        WakeUpManager();
        ~WakeUpManager();

        void Init();
        void Process(void* data, size_t length);
        void SetCallback(wptr<IWakeUpCallback> callback);
        void SetNeedClearBeforeProcess();

    private:
        WakeUpStatus status_;
        int lastVadResult_;

        FILE* file_;

        std::recursive_mutex recursive_mutex_;
        bool needClearBeforeProcess_;

        VadInst* pVad_;
        ps_decoder_t* decoder_;
        cmd_ln_t* config_;

        wptr<IWakeUpCallback> callback_;

        void RunInit();
        void RunDecode(FILE* fp);
        void ClearState();
    };
}
}

#endif