/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_AUDIO_RECORD_MANAGER_H
#define CAR_VOICE_ASSISTANT_AUDIO_RECORD_MANAGER_H

#include "audio_capturer.h"
#include "refbase.h"
#include <chrono>
#include <list>
#include <string>
#include <vector>

namespace OHOS {
namespace CarVoiceAssistant {
    enum AudioRecordStatus {
        AudioRecordStatusNone,
        AudioRecordStatusStarting,
        AudioRecordStatusRunning,
    };

    class IAudioRecordCallback : public virtual RefBase {
    public:
        virtual void AudioRecordStatusChanged(AudioRecordStatus status) = 0;
        virtual void ReceiveAudioBuffer(void* data, size_t length) = 0;
    };

    class AudioRecordManager : public RefBase {
    public:
        AudioRecordManager();
        ~AudioRecordManager();
        
        void SetCallback(wptr<IAudioRecordCallback> callback);

        bool StartRecord();
        void StopRecord();
        AudioRecordStatus GetStatus();

        void OnStateChange(const AudioStandard::CapturerState state);

    private:
        wptr<IAudioRecordCallback> callback_;
        AudioRecordStatus status_;
        bool recordingTag_; //内部标记tag，用于退出取buffer线程

        std::unique_ptr<AudioStandard::AudioCapturer> audioCapturer_;

        void StartCapture();
    };

}
}

#endif