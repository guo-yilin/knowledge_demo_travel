/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_I_WAKEUP_MANAGER_H
#define CAR_VOICE_ASSISTANT_I_WAKEUP_MANAGER_H

#include "refbase.h"

#include <mutex>

namespace OHOS {
namespace CarVoiceAssistant {

    enum WakeUpStatus {
        WakeUpStatusNotInit, //未初始化
        WakeUpStatusInitilazed, //已初始化
        WakeUpStatusRecognizing, // 热词识别中
    };

    class IWakeUpCallback : public virtual RefBase {
    public:
        virtual void WakeUpCallback(std::string text) = 0; //唤醒回调，返回识别的文字
    };

    class IWakeUpManager : public virtual RefBase {
    public:
        IWakeUpManager() = default;
        virtual ~IWakeUpManager() = default;

        virtual void Init() = 0; //唤醒引擎初始化
        virtual void Process(void* data, size_t length) = 0; //处理pcm流
        virtual void SetCallback(wptr<IWakeUpCallback> callback) = 0; //设置回调
        virtual void SetNeedClearBeforeProcess() = 0; //处理pcm流前，先清除当前状态
    };
}
}

#endif