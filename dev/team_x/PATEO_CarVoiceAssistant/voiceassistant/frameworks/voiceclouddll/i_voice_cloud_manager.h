/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_I_VOICE_CLOUD_H
#define CAR_VOICE_ASSISTANT_I_VOICE_CLOUD_H

#include "refbase.h"
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <string>

namespace OHOS {
namespace CarVoiceAssistant {
    enum VoiceCloudStatus {
        VoiceCloudStatusNone, //未连接
        VoiceCloudStatusConnecting, //连接中
        VoiceCloudStatusConnected, // 已连接
        VoiceCloudStatusClosing //关闭中
    };

    struct MemoryStruct {
        char* memory;
        size_t size;
    };

    class IVoiceCloudManagerCallback : public virtual RefBase {
    public:
        virtual void VoiceCloudStatusChanged(VoiceCloudStatus status) = 0;
        virtual void ReveiceVoiceCloudMessage(void* data, size_t length, bool isBinary) = 0;
    };

    class IVoiceCloudManager : public virtual RefBase {
    public:
        IVoiceCloudManager() = default;
        virtual ~IVoiceCloudManager() = default;
        virtual bool Connect() = 0; //连接
        virtual bool Close() = 0; //关闭连接
        virtual VoiceCloudStatus GetStatus() = 0; //获取当前连接状态
        virtual bool IsSendingAudioStream() = 0; //当前是否在发送pcm流
        virtual void SetCallback(wptr<IVoiceCloudManagerCallback> callback) = 0; //设置回调
        virtual void SendNLUText(std::string text) = 0; // 发送文本消息
        virtual void SendStartAudioStream() = 0; // 发送pcm流开始消息
        virtual void SendEndAudioStream() = 0; //发送pcm流结束消息
        virtual void SendBinary(void* data, size_t length) = 0; //发送pcm流
        virtual void SendTrackStat(double latitude, double longitude, std::string& items) = 0; //发送经纬度和热词
        virtual MemoryStruct RequestTTS(std::string text, std::string speakerType) = 0; // 文字转语音pcm流
    };
}
}

#endif