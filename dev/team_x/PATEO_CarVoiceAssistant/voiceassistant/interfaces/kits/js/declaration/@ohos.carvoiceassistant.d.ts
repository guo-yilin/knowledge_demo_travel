/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { AsyncCallback } from './basic';

declare namespace carVoiceAssistant {
    class CarVoiceAssistantManager {

        /**
         * 是否开启唤醒功能
         * @returns true 开启，false 未开启
         */
        isEnableWakeUp(): boolean;

        /**
         * 开启唤醒功能
         * @returns 结果
         */
        enableWakeUp(): ErrorCode;

        /**
           *  关闭唤醒功能
           * @returns 结果
           */
        disableWakeUp(): ErrorCode;

        /**
         * 是否正在识别
         * @returns true 正在识别
         */
        isRecognizing(): boolean;

        /**
         * 开始识别
         * @returns ErrorCode 结果
         */
        startRecognize(): ErrorCode;

        /**
         * 结束识别
         * @returns ErrorCode 结果
         */
        stopRecognize(): ErrorCode;

        /**
        * 播放TTS
        * @returns ErrorCode 结果
        */
        playTTS(tts: string): ErrorCode;

        /**
        * 停止播放TTS
        * @returns ErrorCode 结果
        */
        stopPlayTTS(): ErrorCode;

        /**
         * 注册热词
         * @param hotwords 热词json
         */
        registerHotwords(hotwords: string): void;

        /**
         * 设置经纬度
         * @param latitude 纬度
         * @param longitude 经度
         */
        setCoord(latitude: number, longitude: number): void;

        /**
         * 设置经纬度
         * @param speaker common,zhilingfa,qianranfa,tzruim,gqlanf,
         *                jlshim,madoufp_wenrou,gdfanf_boy,gdfanfp,mandarin,
         *                hchunf_ctn,wqingf_csn,aningf,yukaim_all
         */
        changeSpeakerType(speaker: string): void;

        /**
         * 监听事件
         * @param event 事件类型
         *		
         */
        on(event: EventType.VoiceAssistantEventTypeOnWakeUp, callback: AsyncCallback<{}>): void;
        on(event: EventType.VoiceAssistantEventTypeRecognizeStateChanged, callback: AsyncCallback<{ isRecognizing: boolean }>): void;
        on(event: EventType.VoiceAssistantEventTypeAsrResult, callback: AsyncCallback<{ result: string }>): void;
        on(event: EventType.VoiceAssistantEventTypeTTSPlayStateChanged, callback: AsyncCallback<{ isPlaying: boolean }>): void;

        /**
         * 取消监听
         * @param event 事件类型
         */
        off(event: EventType): void;

    }

    enum ErrorCode {
        VOICE_ASSISTANT_OK = 0, //成功
        VOICE_ASSISTANT_ERR = 8001, //错误
        VOICE_ASSISTANT_START_RECORD_FAILED = 8002, //录音开启失败
        VOICE_ASSISTANT_START_WEBSOCKET_CONNECT_FAILED = 8003, //websocket连接失败
    }

    enum EventType {
        VoiceAssistantEventTypeOnWakeUp = 0, //被唤醒
        VoiceAssistantEventTypeRecognizeStateChanged = 1, //识别状态改变
        VoiceAssistantEventTypeAsrResult = 2, //语音识别结果返回
        VoiceAssistantEventTypeTTSPlayStateChanged = 3, //tts播报状态
    }

    interface AsrResult {
        op: 'realTimeASRResult' | 'nluResult',
        intentName: string, //意图
        text: string, // op为realTimeASRResult时，语音转文字内容；op为nluResult时，语义解析内容
        isFinish: boolean, // op为realTimeASRResult时表示是否结束说话
        url: string, // op为nluResult时，表示响应的热词url
        needDeclare: boolean, //是否多轮
    }

    function getManager(): CarVoiceAssistantManager;
}

export default carVoiceAssistant;