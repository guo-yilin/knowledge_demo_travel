/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VOICE_ASSISTANT_NAPI_TOOLS_H
#define VOICE_ASSISTANT_NAPI_TOOLS_H

#include "napi/native_api.h"
#include "napi/native_node_api.h"

#define MAX_VALUE_LEN 4096

namespace OHOS {
namespace CarVoiceAssistant {
    napi_value WrapVoidToJS(napi_env env);

    napi_value GetUndefinedToJS(napi_env env);

    napi_value GetIntToJs(napi_env env, int number);

    napi_value GetBoolToJs(napi_env env, bool value);

    int GetIntProperty(napi_env env, napi_value obj);

    bool GetBoolProperty(napi_env env, napi_value obj);

    double GetDoubleProperty(napi_env env, napi_value obj);

    int64_t GetLongIntProperty(napi_env env, napi_value obj);

    std::string GetStringProperty(napi_env env, napi_value obj);

    napi_value GetGlobal(napi_env env);
}
}

#endif