/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "voice_assistant_napi_tools.h"
#include "voice_assistant_log.h"

namespace OHOS {
namespace CarVoiceAssistant {
    napi_value WrapVoidToJS(napi_env env)
    {
        napi_value result = nullptr;
        NAPI_CALL(env, napi_get_null(env, &result));
        return (result);
    }

    napi_value GetUndefinedToJS(napi_env env)
    {
        napi_value result = nullptr;
        NAPI_CALL(env, napi_get_undefined(env, &result));
        return (result);
    }

    napi_value GetIntToJs(napi_env env, int number)
    {
        napi_value intToJs = nullptr;
        napi_create_int32(env, number, &intToJs);
        return intToJs;
    }

    napi_value GetBoolToJs(napi_env env, bool value)
    {
        napi_value boolToJs = nullptr;
        napi_get_boolean(env, value, &boolToJs);
        return boolToJs;
    }

    int GetIntProperty(napi_env env, napi_value obj)
    {
        int intTypeToJs = 0;
        if (napi_get_value_int32(env, obj, &intTypeToJs) != napi_ok) {
        }

        return (intTypeToJs);
    }

    bool GetBoolProperty(napi_env env, napi_value obj)
    {
        bool boolTypeToJs = 0;
        if (napi_get_value_bool(env, obj, &boolTypeToJs) != napi_ok) {
        }

        return (boolTypeToJs);
    }

    double GetDoubleProperty(napi_env env, napi_value obj)
    {
        double doubleTypeToJs = 0;
        if (napi_get_value_double(env, obj, &doubleTypeToJs) != napi_ok) {
        }

        return doubleTypeToJs;
    }

    int64_t GetLongIntProperty(napi_env env, napi_value obj)
    {
        int64_t intTypeToJs = 0;
        if (napi_get_value_int64(env, obj, &intTypeToJs) != napi_ok) {
        }

        return (intTypeToJs);
    }

    std::string GetStringProperty(napi_env env, napi_value obj)
    {
        char propValue[MAX_VALUE_LEN] = { 0 };
        size_t propLen;
        if (napi_get_value_string_utf8(env, obj, propValue, MAX_VALUE_LEN,
                &propLen)
            != napi_ok) {
            VOICE_ASSISTANT_LOGI("Can not get string param from argv");
        }

        return (std::string(propValue));
    }

    napi_value GetGlobal(napi_env env)
    {
        napi_value global;
        napi_get_global(env, &global);
        return global;
    }
}
}