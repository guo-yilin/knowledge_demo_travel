/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VOICE_ASSISTANT_EVENT_TARGET_H
#define VOICE_ASSISTANT_EVENT_TARGET_H

#include "napi/native_api.h"
#include "napi/native_node_api.h"
#include "refbase.h"
#include "voice_assistant_log.h"
#include <list>

namespace OHOS {
namespace CarVoiceAssistant {

    enum VoiceAssistantEventType {
        VoiceAssistantEventTypeOnWakeUp,
        VoiceAssistantEventTypeRecognizeStateChanged,
        VoiceAssistantEventTypeAsrResult,
        VoiceAssistantEventTypeTTSPlayStateChanged
    };

    struct VoiceAssistantEventListener {
        napi_env env_;
        VoiceAssistantEventType eventType_;
        napi_ref callbackRef_;
        napi_ref thisVarRef_;
        bool isOnce_;
    };

    class BaseEvent {
    public:
        virtual ~BaseEvent() {};
        virtual napi_value ToJsObject(napi_env env) = 0;
    };

    class RecognizeStateEvent : public BaseEvent {
    public:
        RecognizeStateEvent(bool isRecognizing);
        ~RecognizeStateEvent() = default;
        napi_value ToJsObject(napi_env env);

    private:
        bool isRecognizing_;
    };

    class ArsResultEvent : public BaseEvent {
    public:
        ArsResultEvent(std::string text);
        ~ArsResultEvent() = default;
        napi_value ToJsObject(napi_env env);

    private:
        std::string text_;
    };

    class TTSPlayStateEvent : public BaseEvent {
    public:
        TTSPlayStateEvent(bool isPlaying);
        ~TTSPlayStateEvent() = default;
        napi_value ToJsObject(napi_env env);

    private:
        bool isPlaying_;
    };

    class VoiceAssistantEventTarget : public RefBase {
    public:
        VoiceAssistantEventTarget(napi_env env);
        virtual ~VoiceAssistantEventTarget();
        virtual void On(napi_env env, VoiceAssistantEventType type, napi_value callbackRef, napi_value thisVar);
        virtual void Once(napi_env env, VoiceAssistantEventType type, napi_value callbackRef, napi_value thisVar);
        virtual void Off(napi_env env, VoiceAssistantEventType type, napi_value thisVar);

        void EmitOnWakeUp();
        void EmitRecognizeStateChanged(bool isRecognizing);
        void EmitAsrResult(std::string text);
        void EmitTTSPlayStateChanged(bool isPlaying);

    private:
        std::list<VoiceAssistantEventListener> eventListenerList_;
        napi_env env_;

        virtual void Emit(VoiceAssistantEventType type, BaseEvent* event);
    };

}
}

#endif