# Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

shpath=$(dirname $0)

cd ${shpath}
#解压data.zip
unzip ../data.zip -d ../
#拷贝libvoicecloud.z.so到frameworks/voiceclouddll/目录下
cp ../data/libvoicecloud.z.so ${shpath}/frameworks/voiceclouddll/

#解压zh.tar到resources/目录下
tar xvf ../data/zh.tar -C ${shpath}/resources/

#拷贝voice_tip.mp3到resources/目录下并解压
cp ../data/voice_tip.mp3 ${shpath}/resources/
