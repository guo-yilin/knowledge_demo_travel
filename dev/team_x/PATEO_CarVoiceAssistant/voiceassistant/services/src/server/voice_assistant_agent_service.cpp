/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "voice_assistant_agent_service.h"
#include "voice_assistant_log.h"
#include "ipc_skeleton.h"
#include "iservice_registry.h"
#include "system_ability.h"
#include "common_utils.h"
#include "voice_cloud_loader.h"

using namespace OHOS::CarVoiceAssistant::CommonUtils;

namespace OHOS {
namespace CarVoiceAssistant {

    REGISTER_SYSTEM_ABILITY_BY_ID(VoiceAssistantAgentService, CAR_VOICE_ASSISTANT_SERVICE_SA_ID, true);

    VoiceAssistantAgentService::VoiceAssistantAgentService(int32_t systemAbilityId, bool runOnCreate)
        : SystemAbility(systemAbilityId, runOnCreate)
        , state_(ServiceRunStateNotStart)
    {
    }

    VoiceAssistantAgentService::VoiceAssistantAgentService()
        : state_(ServiceRunStateNotStart)
    {
    }

    VoiceAssistantAgentService::~VoiceAssistantAgentService()
    {
    }

    void VoiceAssistantAgentService::OnStart()
    {
        std::cout<<"Version: 0.0.1"<<std::endl;
        
        VOICE_ASSISTANT_LOGI("OnStart");
        if (state_ == ServiceRunStateRunning) {
            VOICE_ASSISTANT_LOGI("service is already running");
            return;
        }

        Init();
    }

    void VoiceAssistantAgentService::OnStop()
    {
        state_ = ServiceRunStateNotStart;
    }

    int32_t VoiceAssistantAgentService::Init()
    {
        bool ret = Publish(this);
        if (!ret) {
            VOICE_ASSISTANT_LOGI("service publish failed");
            return -1;
        }
        VOICE_ASSISTANT_LOGI("service publish success");

        LoadVoiceCloud();

        state_ = ServiceRunStateRunning;
        return 0;
    }
}
}