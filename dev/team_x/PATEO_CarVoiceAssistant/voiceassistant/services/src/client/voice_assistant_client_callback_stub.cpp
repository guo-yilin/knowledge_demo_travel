/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "voice_assistant_client_callback_stub.h"
#include "common_utils.h"
#include "voice_assistant_client_manager.h"
#include "voice_assistant_log.h"

using namespace OHOS::CarVoiceAssistant::CommonUtils;

namespace OHOS {
namespace CarVoiceAssistant {

    int VoiceAssistantClientCallbackStub::OnRemoteRequest(uint32_t code,
        MessageParcel& data, MessageParcel& reply, MessageOption& option)
    {
        switch (code) {
        case VOICE_ASSITANT_CALLBACK_ON_WAKEUP:
            NotifyWakeUp();
            break;
        case VOICE_ASSITANT_CALLBACK_RECOGNIZE_STATE_CHANGED: {
            bool isRecognizing = data.ReadBool();
            NotifyRecognizeStateChanged(isRecognizing);
        } break;
        case VOICE_ASSITANT_CALLBACK_ASR_RESULT: {
            std::string result = data.ReadString();
            NotifyAsrResult(result);
        } break;
        case VOICE_ASSISTANT_CALLBACK_TTS_STATE_CHANGED: {
            bool isPlaying = data.ReadBool();
            NotifyTTSPlayStateChanged(isPlaying);
        } break;
        default:
            break;
        }

        return 0;
    }

    size_t VoiceAssistantClientCallbackStub::NotifyWakeUp()
    {
        VOICE_ASSISTANT_LOGI("VoiceAssistantClientCallbackStub::NotifyWakeUp");
        sptr<VoiceAssistantEventTarget> eventTarget = VoiceAssistantClientManager::GetInstance()->GetEventTarget();
        if (eventTarget) {
            eventTarget->EmitOnWakeUp();
        }
        return VOICE_ASSISTANT_OK;
    }

    size_t VoiceAssistantClientCallbackStub::NotifyRecognizeStateChanged(bool isRecognizing)
    {
        VOICE_ASSISTANT_LOGI("VoiceAssistantClientCallbackStub::NotifyRecognizeStateChanged");
        sptr<VoiceAssistantEventTarget> eventTarget = VoiceAssistantClientManager::GetInstance()->GetEventTarget();
        if (eventTarget) {
            eventTarget->EmitRecognizeStateChanged(isRecognizing);
        }
        return VOICE_ASSISTANT_OK;
    }

    size_t VoiceAssistantClientCallbackStub::NotifyAsrResult(std::string result)
    {
        VOICE_ASSISTANT_LOGI("VoiceAssistantClientCallbackStub::NotifyAsrResult");
        sptr<VoiceAssistantEventTarget> eventTarget = VoiceAssistantClientManager::GetInstance()->GetEventTarget();
        if (eventTarget) {
            eventTarget->EmitAsrResult(result);
        }
        return VOICE_ASSISTANT_OK;
    }

    size_t VoiceAssistantClientCallbackStub::NotifyTTSPlayStateChanged(bool isPlaying)
    {
        VOICE_ASSISTANT_LOGI("VoiceAssistantClientCallbackStub::NotifyTTSPlayStateChanged");
        sptr<VoiceAssistantEventTarget> eventTarget = VoiceAssistantClientManager::GetInstance()->GetEventTarget();
        if (eventTarget) {
            eventTarget->EmitTTSPlayStateChanged(isPlaying);
        }
        return VOICE_ASSISTANT_OK;
    }
}
}
