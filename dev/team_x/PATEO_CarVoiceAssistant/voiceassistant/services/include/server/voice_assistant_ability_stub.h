/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_ABILITY_STUB_H
#define CAR_VOICE_ASSISTANT_ABILITY_STUB_H

#include "audio_record_manager.h"
#include "common_utils.h"
#include "i_voice_cloud_manager.h"
#include "i_wakeup_manager.h"
#include "iremote_stub.h"
#include "tts_manager.h"
#include "voice_assistant_ability_agent.h"
#include "voice_assistant_callback_event_target.h"
#include <ctime>
#include <mutex>
#include <player.h>

namespace OHOS {
namespace CarVoiceAssistant {

    class VoiceAssistantAbilityAgentStub : public IRemoteStub<IVoiceAssistantAbilityAgent>,
                                           public IVoiceCloudManagerCallback,
                                           public IAudioRecordCallback,
                                           public IWakeUpCallback,
                                           public ITTSManagerCallback {
    public:
        explicit VoiceAssistantAbilityAgentStub();
        virtual ~VoiceAssistantAbilityAgentStub();
        virtual int32_t OnRemoteRequest(uint32_t code,
            MessageParcel& data,
            MessageParcel& reply,
            MessageOption& option) override;

        virtual int32_t IsEnableWakeUp(bool& isEnable) override;
        virtual int32_t EnableWakeUp() override;
        virtual int32_t DisableWakeUp() override;
        virtual int32_t IsRecognizing(bool& isRecognizing) override;
        virtual int32_t StartRecognize(CommonUtils::VoiceAssistantErrorCode& result) override;
        virtual int32_t StopRecognize() override;
        virtual int32_t PlayTTS(CommonUtils::VoiceAssistantErrorCode& result, std::string& tts) override;
        virtual int32_t StopPlayTTS() override;
        virtual int32_t RegisterHotwords(std::string& hotwords) override;
        virtual int32_t SetCoord(double latitude, double longitude) override;
        virtual int32_t RegisterCallback() override;
        virtual int32_t ChangeSpeakerType(std::string speaker) override;

        bool ConnectWebsocket();
        void RemoveCallback(const wptr<IRemoteObject>& remoteObject);

        virtual void VoiceCloudStatusChanged(VoiceCloudStatus status) override;
        virtual void ReveiceVoiceCloudMessage(void* data, size_t length, bool isBinary) override;

        virtual void AudioRecordStatusChanged(AudioRecordStatus status) override;
        virtual void ReceiveAudioBuffer(void* data, size_t length) override;

        virtual void WakeUpCallback(std::string text) override;

        virtual void AudioPlayerStatusChanged(bool isPlaying) override;

        void SendAudioBufferToWebsocketIfNeeded(void* data, size_t length);
        void CheckWakeUpIfNeeded(void* data, size_t length);

    private:
        bool isWakeUpEnabled_; //唤醒功能是否开启
        bool isRecognizing_;
        std::pair<double, double> coord_;
        std::string hotwords_;

        // time_t startRecognizingTime_; //开始识别时间

        std::mutex mutex_;

        sptr<VoiceAssistantCallbackEventTarget> callbackEventTarget_;
        IVoiceCloudManager* voiceCloudManager_;
        sptr<AudioRecordManager> audioRecordManager_;
        sptr<TTSManager> ttsManager_;
        IWakeUpManager* wakeUpManager_;

        std::shared_ptr<OHOS::Media::Player> player_;

        void SendStartAudioStreamIfNeeded();
        void PlayStartRecoginizingSound();
    };
}
}

#endif