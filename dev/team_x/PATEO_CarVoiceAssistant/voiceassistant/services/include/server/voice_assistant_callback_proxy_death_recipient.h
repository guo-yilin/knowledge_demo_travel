/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_CLIENT_CALLBACK_DEATH_RECIPIENT_H
#define CAR_VOICE_ASSISTANT_CLIENT_CALLBACK_DEATH_RECIPIENT_H

#include "iremote_object.h"
#include "refbase.h"

namespace OHOS
{
    namespace CarVoiceAssistant
    {
        class VoiceAssistantClientCallbackDeathRecipient : public IRemoteObject::DeathRecipient
        {
        public:
            VoiceAssistantClientCallbackDeathRecipient() = default;
            virtual ~VoiceAssistantClientCallbackDeathRecipient() = default;

            virtual void OnRemoteDied(const wptr<IRemoteObject> &remote)
            {
                if (diedCb_ != nullptr)
                {
                    diedCb_(remote);
                }
            }

            using NotifyFunc = std::function<void(const wptr<IRemoteObject> &)>;
            void SetNotifyCb(NotifyFunc func)
            {
                diedCb_ = func;
            }

        private:
            NotifyFunc diedCb_ = nullptr;
        };
    }
}

#endif