/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_CLIENT_CALLBACK_PROXY_H
#define CAR_VOICE_ASSISTANT_CLIENT_CALLBACK_PROXY_H

#include "voice_assistant_client_callback.h"
#include "common_utils.h"
#include "iremote_object.h"
#include "iremote_proxy.h"

namespace OHOS {
namespace CarVoiceAssistant {
    class VoiceAssistantClientCallbackProxy : public IRemoteProxy<IVoiceAssistantClientCallback> {
    public:
        explicit VoiceAssistantClientCallbackProxy(const sptr<IRemoteObject>& impl);

        virtual size_t NotifyWakeUp() override;
        virtual size_t NotifyRecognizeStateChanged(bool isRecognizing) override;
        virtual size_t NotifyAsrResult(std::string result) override;
        virtual size_t NotifyTTSPlayStateChanged(bool isPlaying) override;

    private:
        static inline BrokerDelegator<VoiceAssistantClientCallbackProxy> delegator_;
        CommonUtils::VoiceAssistantErrorCode DoDispatch(uint32_t cmd, MessageParcel& data, MessageParcel& reply);
    };
}
}

#endif