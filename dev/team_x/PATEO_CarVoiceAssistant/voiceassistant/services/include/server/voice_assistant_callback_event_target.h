/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAR_VOICE_ASSISTANT_CALLBACK_EVENT_TARGET_H
#define CAR_VOICE_ASSISTANT_CALLBACK_EVENT_TARGET_H

#include "voice_assistant_callback_proxy_death_recipient.h"
#include "voice_assistant_client_callback.h"
#include "refbase.h"
#include <list>

namespace OHOS {
namespace CarVoiceAssistant {
    struct VoiceAssistantCallbackEventListener {
        sptr<IVoiceAssistantClientCallback> proxy_;
        sptr<VoiceAssistantClientCallbackDeathRecipient> deathRecipient_;
    };

    class VoiceAssistantCallbackEventTarget : public RefBase {
    public:
        void AddListener(sptr<IVoiceAssistantClientCallback> proxy, sptr<VoiceAssistantClientCallbackDeathRecipient> deathRecipient);
        void RemoveListener(const wptr<IRemoteObject>& remote);

        void EmitOnWakeUp();
        void EmitRecognizeStateChanged(bool isRecognizing);
        void EmitAsrResult(std::string& result);
        void EmitTTSPlayStateChanged(bool isPlaying);
        template <typename Callback>
        void DoEmit(Callback callback);

    private:
        std::list<VoiceAssistantCallbackEventListener> listenerList_;
    };

}
}

#endif