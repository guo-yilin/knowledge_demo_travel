/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_AGENT_SERVICE_H
#define CAR_VOICE_ASSISTANT_AGENT_SERVICE_H

#include "voice_assistant_ability_stub.h"
#include "system_ability.h"

namespace OHOS {
namespace CarVoiceAssistant {

    enum ServiceRunState {
        ServiceRunStateNotStart,
        ServiceRunStateRunning
    };

    class VoiceAssistantAgentService : public SystemAbility,
        public VoiceAssistantAbilityAgentStub {
        DECLARE_SYSTEM_ABILITY(VoiceAssistantAgentService);

    public:
        DISALLOW_COPY_AND_MOVE(VoiceAssistantAgentService);
        VoiceAssistantAgentService(int32_t systemAbilityId, bool runOnCreate);
        VoiceAssistantAgentService();
        ~VoiceAssistantAgentService();

    protected:
        void OnStart() override;
        void OnStop() override;

    private:
        ServiceRunState state_;
        int32_t Init();
    };
}
}

#endif