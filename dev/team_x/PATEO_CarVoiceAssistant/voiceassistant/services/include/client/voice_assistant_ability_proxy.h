/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_ABILITY_PROXY_H
#define CAR_VOICE_ASSISTANT_ABILITY_PROXY_H

#include "voice_assistant_ability_agent.h"
#include "common_utils.h"
#include "iremote_proxy.h"
#include "refbase.h"

namespace OHOS {
namespace CarVoiceAssistant {
    class VoiceAssistantAbilityAgentProxy : public IRemoteProxy<IVoiceAssistantAbilityAgent> {
    public:
        explicit VoiceAssistantAbilityAgentProxy(const sptr<IRemoteObject>& object);
        ~VoiceAssistantAbilityAgentProxy() = default;
        virtual int32_t IsEnableWakeUp(bool& isEnable) override;
        virtual int32_t EnableWakeUp() override;
        virtual int32_t DisableWakeUp() override;
        virtual int32_t IsRecognizing(bool& isRecognizing) override;
        virtual int32_t StartRecognize(CommonUtils::VoiceAssistantErrorCode& result) override;
        virtual int32_t StopRecognize() override;
        virtual int32_t PlayTTS(CommonUtils::VoiceAssistantErrorCode& result,std::string& tts) override;
        virtual int32_t StopPlayTTS() override;
        virtual int32_t RegisterHotwords(std::string& hotwords) override;
        virtual int32_t SetCoord(double latitude, double longitude) override;
        virtual int32_t RegisterCallback() override;
        virtual int32_t ChangeSpeakerType(std::string speaker) override;

    private:
        static inline BrokerDelegator<VoiceAssistantAbilityAgentProxy> delegator_;
        CommonUtils::VoiceAssistantErrorCode DoDispatch(uint32_t cmd, MessageParcel& data, MessageParcel& reply);
    };
}
}

#endif