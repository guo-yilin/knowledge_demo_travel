/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_CLIENT_CALLBACK_H
#define CAR_VOICE_ASSISTANT_CLIENT_CALLBACK_H

#include "iremote_broker.h"
#include <string>

namespace OHOS {
namespace CarVoiceAssistant {
    class IVoiceAssistantClientCallback : public IRemoteBroker {
    public:
        enum {
            VOICE_ASSITANT_CALLBACK_ON_WAKEUP, //被唤醒
            VOICE_ASSITANT_CALLBACK_RECOGNIZE_STATE_CHANGED, //识别状态改变
            VOICE_ASSITANT_CALLBACK_ASR_RESULT, //语音识别结果返回
            VOICE_ASSISTANT_CALLBACK_TTS_STATE_CHANGED, // tts播报状态改变
        };

        virtual size_t NotifyWakeUp() = 0;
        virtual size_t NotifyRecognizeStateChanged(bool isRecognizing) = 0;
        virtual size_t NotifyAsrResult(std::string result) = 0;
        virtual size_t NotifyTTSPlayStateChanged(bool isPlaying) = 0;

        DECLARE_INTERFACE_DESCRIPTOR(u"ohos.miscservices.voiceassistant.IVoiceAssistantClientCallback");
    };
}
}

#endif