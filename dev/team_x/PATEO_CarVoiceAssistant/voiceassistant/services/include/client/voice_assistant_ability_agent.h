/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CAR_VOICE_ASSISTANT_ABILITY_AGENT_H
#define CAR_VOICE_ASSISTANT_ABILITY_AGENT_H

#include <list>
#include <map>
#include <string>

#include "iremote_broker.h"
#include "common_utils.h"

namespace OHOS {
namespace CarVoiceAssistant {
    class IVoiceAssistantAbilityAgent : public IRemoteBroker {
    public:
        enum {
            VOICE_ASSITANT_CMD_IS_ENABLE_WAKEUP = 0, //是否开启了唤醒
            VOICE_ASSITANT_CMD_ENABLE_WAKEUP, //开启唤醒
            VOICE_ASSITANT_CMD_DISABLE_WAKEUP, //取消唤醒
            VOICE_ASSITANT_CMD_IS_RECOGNIZING, //是否识别中
            VOICE_ASSITANT_CMD_START_RECOGNIZE, //开始识别
            VOICE_ASSITANT_CMD_STOP_RECOGNIZE, //停止识别
            VOICE_ASSITANT_CMD_PLAY_TTS, //播放TTS
            VOICE_ASSITANT_CMD_STOP_PLAY_TTS, //停止播放TTS
            VOICE_ASSITANT_CMD_REGISTER_HOTWORDS, //注册热词
            VOICE_ASSISTANT_CMD_SET_COORD, //设置经纬度
            VOICE_ASSITANT_CMD_REGISTER_CALLBACK, //注册回调
            VOICE_ASSITANT_CMD_CHANGE_SPEAKER_TYPE, //修改TTS播报声音
        };

        virtual int32_t IsEnableWakeUp(bool& isEnable) = 0;
        virtual int32_t EnableWakeUp() = 0;
        virtual int32_t DisableWakeUp() = 0;
        virtual int32_t IsRecognizing(bool& isRecognizing) = 0;
        virtual int32_t StartRecognize(CommonUtils::VoiceAssistantErrorCode& result) = 0;
        virtual int32_t StopRecognize() = 0;
        virtual int32_t PlayTTS(CommonUtils::VoiceAssistantErrorCode& result, std::string& tts) = 0;
        virtual int32_t StopPlayTTS() = 0;
        virtual int32_t RegisterHotwords(std::string& hotwords) = 0;
        virtual int32_t SetCoord(double latitude, double longitude) = 0;
        virtual int32_t RegisterCallback() = 0;
        virtual int32_t ChangeSpeakerType(std::string speaker) = 0;

        DECLARE_INTERFACE_DESCRIPTOR(u"ohos.miscservices.voiceassistant.IVoiceAssistantAbilityAgent");
    };
} // namespace CarVoiceAssistant
} // namespace OHOS

#endif