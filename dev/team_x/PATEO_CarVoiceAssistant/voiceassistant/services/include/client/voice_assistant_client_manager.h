/*
 * Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef VOICE_ASSISTANT_CLIENT_MANAGER_H
#define VOICE_ASSISTANT_CLIENT_MANAGER_H

#include "voice_assistant_ability_proxy.h"
#include "voice_assistant_agent_proxy_death_recipient.h"
#include "voice_assistant_event_target.h"
#include "common_utils.h"
#include "refbase.h"
#include <mutex>

namespace OHOS {
namespace CarVoiceAssistant {

    class VoiceAssistantClientManager : public RefBase {
    public:
        static sptr<VoiceAssistantClientManager> GetInstance();

        VoiceAssistantClientManager();
        ~VoiceAssistantClientManager();

        void SetEventTarget(sptr<VoiceAssistantEventTarget> eventTarget);
        sptr<VoiceAssistantEventTarget> GetEventTarget();

        int32_t IsEnableWakeUp(bool& isEnable);
        int32_t EnableWakeUp();
        int32_t DisableWakeUp();
        int32_t IsRecognizing(bool& isRecognizing);
        int32_t StartRecognize(CommonUtils::VoiceAssistantErrorCode& result);
        int32_t StopRecognize();
        int32_t PlayTTS(CommonUtils::VoiceAssistantErrorCode& result,std::string& tts);
        int32_t StopPlayTTS();
        int32_t RegisterHotwords(std::string& hotwords);
        int32_t SetCoord(double latitude, double longitude);
        int32_t RegisterCallback();
        int32_t ChangeSpeakerType(std::string speaker);

    private:
        sptr<VoiceAssistantEventTarget> eventTarget_;
        sptr<IVoiceAssistantAbilityAgent> mAbilityManager_;
        sptr<VoiceAssistantAgentProxyDeathRecipient> deathRecipient_;
        std::mutex createProxyLock_;

        static std::mutex instanceLock_;
        static sptr<VoiceAssistantClientManager> instance_;

        sptr<IVoiceAssistantAbilityAgent> CreateAbilityAgentProxy();
        void ResetAgentProxy();
    };

}
}

#endif