# Copyright (c) 2022 PATEO CONNECT+ (Nanjing) Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")

ohos_shared_library("carvoiceassistant") {
    cflags = ["-Wno-unused-variable", "-Wno-unused-function", "-Wno-implicit-function-declaration", "-Wno-unused-private-field"]
    cflags_cc = ["-fexceptions"]

    include_dirs = [
        "//base/miscservices/voiceassistant/frameworks/utils/include",
        "//base/miscservices/voiceassistant/interfaces/kits/js/napi/include",
        "//base/miscservices/voiceassistant/services/include/client",
        "//foundation/ace/napi/interfaces/kits",
        "//utils/native/base/include",
        "//utils/system/safwk/native/include",
        "//third_party/openssl/include",
        "//third_party/json/single_include",
        "//third_party"
    ]

    sources = [
        "src/client/voice_assistant_ability_proxy.cpp",
        "src/client/voice_assistant_client_callback_stub.cpp",
        "src/client/voice_assistant_client_manager.cpp",
        "../interfaces\kits\js\napi\src\voice_assistant_event_target.cpp",
        "../interfaces\kits\js\napi\src\voice_assistant_napi_tools.cpp",
        "../interfaces\kits\js\napi\src\voice_assistant_napi.cpp"
    ]

    deps = [ 
        "//foundation/ace/napi/:ace_napi",
        "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_base:appexecfwk_base",
        "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_core:appexecfwk_core",
        "//foundation/appexecfwk/standard/interfaces/innerkits/libeventhandler:libeventhandler",
        "//foundation/communication/ipc/interfaces/innerkits/ipc_core:ipc_core",
        "//foundation/communication/ipc/interfaces/innerkits/ipc_single:ipc_single",
        "//foundation/distributedschedule/dmsfwk/interfaces/innerkits/uri:zuri",
        "//foundation/distributedschedule/samgr/interfaces/innerkits/samgr_proxy:samgr_proxy",
        "//utils/native/base:utils",
        "//third_party/libwebsockets:websockets",
        "//third_party/openssl:libcrypto_static",
        "//third_party/openssl:ssl_source",
        "//third_party/zlib:libz",
    ]

    external_deps = [
        "hiviewdfx_hilog_native:libhilog"
    ]

    subsystem_name = "miscservices"
    part_name = "voiceassistant"
}

ohos_shared_library("voiceassistant_service") {
    cflags = ["-Wno-unused-variable", "-Wno-unused-function", "-Wno-implicit-function-declaration", "-Wno-unused-private-field"]
    cflags_cc = ["-fexceptions"]

    include_dirs = [
        "//foundation/multimedia/audio_standard/interfaces/inner_api/native/audiocapturer/include",
        "//foundation/multimedia/audio_standard/interfaces/inner_api/native/audiocommon/include",
        "//base/miscservices/voiceassistant/frameworks/manager/include",
        "//base/miscservices/voiceassistant/frameworks/pocketsphinx/include",
        "//base/miscservices/voiceassistant/frameworks/vad/include",
        "//base/miscservices/voiceassistant/frameworks/utils/include",
        "//base/miscservices/voiceassistant/frameworks/websocket/include",
        "//base/miscservices/voiceassistant/services/include/client",
        "//base/miscservices/voiceassistant/services/include/server",
        "//base/miscservices/voiceassistant/frameworks/voiceclouddll",
        "//utils/native/base/include",
        "//third_party/libwebsockets/include",
        "//third_party/openssl/include",
        "//third_party/json/single_include",
        "//third_party/curl/include",
        "//foundation/graphic/standard/interfaces/innerkits/wmclient",
        "//foundation/graphic/standard/interfaces/innerkits/common",
        "//foundation/multimedia/media_standard/interfaces/innerkits/native/media/include",
        "//foundation/multimedia/media_standard/services/utils/include"
    ]

    sources = [
        "../frameworks/manager/src/audio_record_manager.cpp",
        "../frameworks/manager/src/tts_manager.cpp",
        "../frameworks/manager/src/wakeup_manager.cpp",
        "../frameworks/manager/src/voice_cloud_loader.cpp",
        "src/server/voice_assistant_ability_stub.cpp",
        "src/server/voice_assistant_agent_service.cpp",
        "src/server/voice_assistant_callback_event_target.cpp",
        "src/server/voice_assistant_client_callback_proxy.cpp"
    ]

    deps = [ 
        "//base/miscservices/voiceassistant/frameworks/pocketsphinx:pocketsphinx",
        "//base/miscservices/voiceassistant/frameworks/vad:ps_vad",
        "//foundation/ace/napi/:ace_napi",
        "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_base:appexecfwk_base",
        "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_core:appexecfwk_core",
        "//foundation/appexecfwk/standard/interfaces/innerkits/libeventhandler:libeventhandler",
        "//foundation/communication/ipc/interfaces/innerkits/ipc_core:ipc_core",
        "//foundation/communication/ipc/interfaces/innerkits/ipc_single:ipc_single",
        "//foundation/distributedschedule/dmsfwk/interfaces/innerkits/uri:zuri",
        "//foundation/distributedschedule/safwk/interfaces/innerkits/safwk:system_ability_fwk",
        "//foundation/distributedschedule/samgr/interfaces/innerkits/samgr_proxy:samgr_proxy",
        "//foundation/distributeddatamgr/appdatamgr/interfaces/innerkits/native_preferences:native_preferences",
        "//utils/native/base:utils",
        "//third_party/libwebsockets:websockets",
        "//third_party/openssl:libcrypto_static",
        "//third_party/openssl:ssl_source",
        "//third_party/zlib:libz",
        "//foundation/multimedia/audio_standard/interfaces/inner_api/native/audiocapturer:audio_capturer",
        "//foundation/graphic/standard:libsurface",
        "//foundation/graphic/standard:libwmclient",
        "//foundation/graphic/standard/frameworks/surface:surface",
        "//foundation/multimedia/image_standard/interfaces/innerkits:image_native",
        "//third_party/libjpeg:libjpeg_static",
        "//third_party/curl:curl"
    ]

    external_deps = [
        "hiviewdfx_hilog_native:libhilog",
        "multimedia_media_standard:media_client"
    ]

    subsystem_name = "miscservices"
    part_name = "voiceassistant"
}

group("voiceassistant_service_group") {
    deps = [":carvoiceassistant",
            ":voiceassistant_service"]
}