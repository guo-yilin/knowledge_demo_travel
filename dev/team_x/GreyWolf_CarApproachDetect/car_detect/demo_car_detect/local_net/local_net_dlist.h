/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOCAL_NET_DLIST_H
#define LOCAL_NET_DLIST_H

#ifdef __cplusplus
extern "C" {
#endif

/****************
 * 功能：显示程序异常所在位置、内容的宏
 * 参数：x(待比较的第一个参数), option(比较符号如：> =), y(待比较的第二个参数)，message(自定义的异常内容)，
 *       z(自定义的返回值)
 * 返回值: z ：NULL 表示数据为空，-1表示其他异常
 * *************/
#define SYSERR(x, option, y, message, z)                                           \
    if ((x)option(y)) {                                                            \
        printf("\t%s %s %d : error: %s\n", __FILE__, __func__, __LINE__, message); \
        return z;                                                                  \
    }

typedef struct Node { //链表节点
    struct Node *before; //前向指针
    void *data;          //数据指针　
    struct Node *after;  //后向指针
} Node_t;

/****************
 * 功能：创建双向链表
 * 参数：无
 * 返回值：指向该被创建的双向链表头节点的指针
 * *************/
Node_t *CreateDlist();

/****************
 * 功能：链表头部插入数据
 * 参数：head:链表头部指针， newvalue:指向该新添加数据的指针， size:该新添加数据的大小
 * 返回值：0添加失败 1添加成功
 * *************/
int InsertHdlist(Node_t *head, const void *newdata, int size);

/****************
 * 功能：链表尾部插入数据
 * 参数：head:链表头部指针， newvalue:指向该新添加数据的指针， size:该新添加数据的大小
 * 返回值：0添加失败 1添加成功
 * *************/
int InsertTdlist(Node_t *head, const void *newdata, int size);

/****************
 * 功能：指向比较函数的函数指针
 * 参数：data1：指向第一个数据的指针，data2：指向第二个数据的指针
 * 返回值：大于0：data1大于data2, 小于0：与前面相反， 等于0：data1和data2相等
 ***************/
typedef int (*CmpFun_t)(const void *data1, const void *data2);

/****************
 * 功能：按值查找数据所在链表的节点
 * 参数：head链表头部指针， value:目标节点所包含的数据， cmpFun:指向比较函数指针
 * 返回值：指向目标数据所在节点的指针
 * *************/
Node_t *FindVdlist(Node_t *head, const void *value, CmpFun_t cmpFun);

/****************
 * 功能：选择排序该链表
 * 参数：head:链表头部指针，cmpFun:比较函数指针
 * 返回值：无
 * *************/
void SelectSortdlist(Node_t *head, CmpFun_t cmpFun);

/****************
 * 功能：指向打印函数的函数指针
 * 参数：data：指向节点数据的指针
 * 返回值：无
 ***************/
typedef void (*PrintNodeFun_t)(const void *data);

/****************
 * 功能：打印所在链表的所有节点数据
 * 参数：head链表头部指针， PrintNodeFun_t:指向节点打印函数指针
 * 返回值：无
 * *************/
void Printdlist(Node_t *head, PrintNodeFun_t printFun);

/****************
 * 功能：在链表头部删除一个节点
 * 参数：head:链表头部指针
 * 返回值：0删除失败，1删除成功
 * *************/
int DeleteHdlist(Node_t *head);

/****************
 * 功能：在链表尾部删除一个节点
 * 参数：head:链表头部指针
 * 返回值：0删除失败，1删除成功
 * *************/
int DeleteTdlist(Node_t *head);

/****************
 * 功能：按值删除链表中的节点
 * 参数：head:链表头部指针， value:被删除节点所包含的数据， cmpFun:比较函数指针
 * 返回值：该链表头节点指针
 * *************/
Node_t *DeleteVdlist(Node_t *head, const void *value, CmpFun_t cmpFun);

/****************
 * 功能：删除链表
 * 参数: head指向该链表头节点的二级指针
 * 返回值：1：删除成功，0:删除失败
 * *************/
int DestroyDlist(Node_t **head);

#ifdef __cplusplus
}
#endif

#endif // LOCAL_NET_DLIST_H
