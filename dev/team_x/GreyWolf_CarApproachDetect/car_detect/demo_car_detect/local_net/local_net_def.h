/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOCAL_NET_DEF_H
#define LOCAL_NET_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEVICE_ID_LEN 65
#define MQ_MSG_SIZE_MAX 4
#define MQ_MSG_NUM_MAX 10

#define L0_DEVICE 1

#define LOG_D(fmt, ...) printf("[%s] %d: " fmt "\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_W(fmt, ...) printf("[%s] %d: " fmt "\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_I(fmt, ...) printf("[%s] %d: " fmt "\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOG_E(fmt, ...) printf("[%s] %d: " fmt "\r\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define DBG_ASSERT(cond)                                                        \
    do {                                                                        \
        if (!(cond)) {                                                          \
            printf("%s:%d '%s' assert failed.\r\n", __FILE__, __LINE__, #cond); \
            while (1)                                                           \
                ;                                                               \
        }                                                                       \
    } while (0)

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

enum ResponseState {
    /**
     * 控制命令响应状态值.
     */
    SUCCESS = 200,
    ERROR = 500
};

typedef enum {
    LOCAL_NET_THREAD_INIT = 0,
    LOCAL_NET_THREAD_RUNNING,
    LOCAL_NET_THREAD_RELEASE,
    LOCAL_NET_THREAD_EXIT,
    LOCAL_NET_THREAD_ERR
} LocalNetThreadStatus;

#ifdef __cplusplus
}
#endif

#endif
