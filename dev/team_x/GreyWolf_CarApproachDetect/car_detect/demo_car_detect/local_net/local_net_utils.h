/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __LOCAL_NET_UTILS_H__
#define __LOCAL_NET_UTILS_H__

/**
 * @brief 计算字符串SHA-256
 *
 * @param str 字符串指针
 * @param length 字符串长度
 * @param sha256 用于保存SHA-256的字符串指针
 * @return int 成功返回0
 */
int StrSha256(char *str, long length, char *sha256);

/**
 * @brief 根据硬件网卡名称，获取本机ip
 *
 * @param eth_name 硬件名称
 * @param local_ip_addr 所获取的ip地址
 * @return int 1：成功，0失败；
 */
int getLocalIp(const char *eth_name, char *local_ip_addr);

#ifdef L2_DEVICE
int initLogs(const char *logRoute);

int addLogs(const char *logs);

int closeLogs();

#define PRINT_TIME 0
#ifndef u32
#define u32 unsigned int
#endif

u32 getMillionSecond(void);
#endif

#endif // __LOCAL_NET_UTILS_H__

#ifdef __cplusplus
}
#endif