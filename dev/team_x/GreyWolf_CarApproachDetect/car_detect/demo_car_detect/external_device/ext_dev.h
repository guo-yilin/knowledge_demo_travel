/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __EXT_DEV_H__
#define __EXT_DEV_H__

#include "ext_dev_if.h"
#include "log.h"

#undef LOG_TAG
#define LOG_TAG "EXT_DEV"

#define LOG_D(fmt, ...) HILOG_DEBUG(HILOG_MODULE_APP, fmt, ##__VA_ARGS__)
#define LOG_W(fmt, ...) HILOG_WARN(HILOG_MODULE_APP, fmt, ##__VA_ARGS__)
#define LOG_I(fmt, ...) HILOG_INFO(HILOG_MODULE_APP, fmt, ##__VA_ARGS__)
#define LOG_E(fmt, ...) HILOG_ERROR(HILOG_MODULE_APP, fmt, ##__VA_ARGS__)
#define DBG_ASSERT(cond)                                                             \
    do {                                                                             \
        if (!(cond)) {                                                               \
            HILOG_ERROR("%s:%d '%s' assert failed.\r\n", __FILE__, __LINE__, #cond); \
            while (1)                                                                \
                ;                                                                    \
        }                                                                            \
    } while (0)

#endif // __EXT_DEV_H__
