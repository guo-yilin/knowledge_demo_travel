/*
# Copyright (c) 2020-2030 iSoftStone Information Technology (Group) Co.,Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
*/
import seetaface from '@ohos.napi_seetaface'
import file from '@system.file'
import {describe, it, expect} from 'deccjsunit/index'


const LOCAL_FACE_ROOT = '/data/accounts/account_0/appdata/com.example.facedetect/files/'
const LOCAL_FACE_PATH = 'internal://app/files'

describe('FaceDetectTest', function () {
    it("test_clearLocationData", 0, function () {
        let rst = -1
        seetaface.ClearFaceDatabase()
        rst = 0
        expect(rst).assertEqual(0)
    })
    it('test_RegisterImage', 0, function () {
        let rst = false
        file.list({
            uri: LOCAL_FACE_PATH,
            success: function (data) {
                var fileList = data.fileList
                log('registerFaceId -- fileList --' + fileList)
                var length = fileList.length
                log('registerFaceId -- ' + 'length -- ' + length)
                expect(length > 0).assertTrue()
                for (var i = 0; i < length; i++) {
                    var fileUri = fileList[i].uri
                    var fileName = fileUri.substr(fileUri.lastIndexOf('/'))
                    var imgPath = LOCAL_FACE_ROOT + fileName
                    var last = imgPath.substr(imgPath.lastIndexOf(".")).toLowerCase()
                    expect(last == '.jpg' || last == '.png').assertTrue()
                    if (last == '.jpg' || last == '.png') {
                        var id = seetaface.RegisterImage(imgPath)
                        expect(id >= 0).assertTrue()
                    }
                }
            },
            fail: function (data, code) {
                log('registerFaceId -- fail -- ' + 'code -- ' + code)
                expect(rst).assertTrue()
            }
        })
    })
})

function log(txt) {
    console.log('FaceDetectTest - ' + txt)
}