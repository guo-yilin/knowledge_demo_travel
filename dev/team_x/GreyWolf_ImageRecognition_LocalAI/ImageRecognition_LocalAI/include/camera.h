/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CAMERA_H
#define __CAMERA_H
    #define IMG_PATH "/sdcard/CaptureAi.jpg"
    static const int SSID_LEN = 64;
    typedef struct {
        char ssid[SSID_LEN];
        char pwd[SSID_LEN];
    } WifiInfo;

    void Beep(char *videoPath);
    int GetScanSsidPassword(WifiInfo &wifi, char *gpBuf, int size);
    int InitCamera(void);
    void RunAICamera(void);
    void RunQRCamera(void);
#endif
