/*
# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#pragma once

#include <opencv2/core.hpp>
#include <seeta/CStruct.h>

namespace seeta
{
    namespace cv
    {
        class ImageData : public SeetaImageData {
        public:
            ImageData(const ::cv::Mat &mat)
                : cv_mat(mat.clone()) {
                this->width = cv_mat.cols;
                this->height = cv_mat.rows;
                this->channels = cv_mat.channels();
                this->data = cv_mat.data;
            }

            ImageData(int width, int height, int channels = DEFAULT_CHNNAL)
                : cv_mat(height, width, CV_8UC(channels)) {
                this->width = cv_mat.cols;
                this->height = cv_mat.rows;
                this->channels = cv_mat.channels();
                this->data = cv_mat.data;
            }
            ImageData(const SeetaImageData &img)
                : cv_mat(img.height, img.width, CV_8UC(img.channels), img.data) {
                this->width = cv_mat.cols;
                this->height = cv_mat.rows;
                this->channels = cv_mat.channels();
                this->data = cv_mat.data;
            }
            ImageData()
                : cv_mat() {
                this->width = cv_mat.cols;
                this->height = cv_mat.rows;
                this->channels = cv_mat.channels();
                this->data = cv_mat.data;
            }
            bool empty() const {
                return cv_mat.empty();
            }
            operator ::cv::Mat() const {
                return cv_mat.clone();
            }
            ::cv::Mat toMat() const {
                return cv_mat.clone();
            }
        private:
            ::cv::Mat cv_mat;

            const static int16_t DEFAULT_CHNNAL = 3;
        };
    }
}
