# [智能停车系统之车牌识别器（云识别版本）](../../dev/team_x/GreyWolf_ImageRecognition_RemoteAI)

## 简介

本文介绍如何在OpenHarmony-3.1-Beta轻量系统在系统润和Hi3518开发板上实现[远端车牌识别](../../dev/team_x/GreyWolf_ImageRecognition_RemoteAI)。该项目分为二维码扫码配网、语音播放、相机拍照、远程调用百度云识别车牌接口、获取识别结果并通过串口显示。

![](media/display.gif)

## 快速上手

### 硬件准备

润和Hi3518（HiSpark Aries IPC 摄像头开发板套件）

准备8GSD卡插入Hi3518E插槽。

能连接外网的路由器。

### 下载源码

#### 准备

获取源码及Ubuntu编译环境准备

开发基础环境由windows 工作台和Linux  编译服务器组成。windows 工作台可以通过samba 服务或ssh  方式访问Linux编译服务器。其中windows  工作台用来烧录和代码编辑，Linux编译服务器用来编译OpenHarmony代码，为了简化步骤，Linux编译服务器推荐安装Ubuntu20.04。

[操作文档](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/device-dev/quick-start/quickstart-lite-env-setup.md#/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/device-dev/quick-start/quickstart-lite-env-setup-overview.md)

润和Hi3518开发环境准备

在Linux编译服务器上搭建好基础开发环境后，需要安装OpenHarmony 编译润和Hi3518 平台特有的开发环境。

[操作文档](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3518.md)

#### 下载OpenHarmony源码

##### 码云工具下载

```
cd ~/
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > ./repo
sudo cp repo /usr/local/bin/repo
chmod a+x /usr/local/bin/repo
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
```

##### 代码下载

[远程车牌识别demo](../../dev/team_x/GreyWolf_ImageRecognition_RemoteAI)适配OpenHarmony-3.1-Beta版本。

新建文件夹

```
mkdir ~/OpenHarmony
cd ~/OpenHarmony
```

OpenHarmony-3.1-Beta下载：

```
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

下载GreyWolf_ImageRecognition_RemoteAI代码

具体仓库地址: [knowledge_demo_travel](https://gitee.com/openharmony-sig/knowledge_demo_travel)

通过git命令下载(方便后期代码上传管理，建议先将仓库fork到自己账号目录，然后再下载)：

```
git clone git@gitee.com:xxxxxxx/knowledge_demo_travel.git
其中xxxxxxx为fork后相关账号名字。
```

#### 代码拷贝

##### 准备

二维码扫码识别、语音播放参考文档[openharmony hi3518开发板实现扫码](https://ost.51cto.com/posts/8653) [openharmony hi3518开发板实现扫码后添加声音](https://ost.51cto.com/posts/8662)下载 zxing三方库放到third_party下。

车牌识别服务器使用的是百度云，由于OpenHarmony-3.1-Beta中的curl库不支持L1因此无法使用官方demo，现在使用代理服务器的方式实现。首先参考官方视频[获取AK_SK](https://abcxueyuan.baidu.com/#/play_video?id=15431&courseId=15431&mediaId=mda-mhsry0xzw271d5g2&videoId=4540&sectionId=15685&type=%E5%85%8D%E8%B4%B9%E8%AF%BE%E7%A8%8B&showCoursePurchaseStatus=false)。在源码的http.cpp中的http函数中更改。

![ak_sk](media/ak_sk.png)

进入knowledge_demo_travel/dev/team_x下

拷贝远程AI实现代码

```
cp -rf ImageRecognitionRemoteAI/ ~/OpenHarmony/applications/sample/camera/ 
```

更改启动文件init_liteos_a_3518ev300.cfg将应用设置为开机自启。

```
cp -f config/init_liteos_a_3518ev300.cfg  ~/OpenHarmony/vendor/hisilicon/hispark_aries/init_configs/init_liteos_a_3518ev300.cfg 
```

更改appapplications.json与config.json将应用加入到编译体系中来

```
cp -f config/applications.json  ~/OpenHarmony/build/lite/components/applications.json
cp -f config/config.json ~/OpenHarmony/vendor/hisilicon/hispark_aries/config.json
```

### 编译

hb set 选择ipcamera_hispark_aries

```
hb build -f  -- 开始全量编译。（hb build 为增量编译）
```

### 固件烧录

##### 烧录工具选择

固件编译完后，是需要烧录到单板的。这里我们用的是HiTool工具烧录的。(HiTool工具下载地址:[HiHope官网](https://gitee.com/link?target=http%3A%2F%2Fwww.hihope.org%2Fdownload%2Fdownload.aspx%3Fmtt%3D33))

##### 烧录步骤

打开HiTool工具，如下图：

烧写步骤按照图中标注即可。点击擦除后再拔出USB口再接入。![hitool_config](media/hitool_config.png)

显示擦除成功后，然后按如下图选择，再点击烧写。

![](media/hitoo_burn.png)

烧录成功后，使用串口连接。输入如下启动参数。

```
setenv bootcmd "sf probe 0;sf read 0x40000000 0x100000 0x500000;go 0x40000000";
setenv bootargs "console=ttyAMA0,115200n8 root=flash fstype=jffs2 rw rootaddr=6M rootsize=9M";
save;
reset
```

## 启动应用

#### 准备

将源码下的sdcard文件拷贝SD卡中。

在烧录前使用HiSpark中的指导文档对摄像头调整焦距，将摄像头调整识别二维码为距离为15cm。

[云盘资料链接]( https://pan.baidu.com/s/1uiiCpbJqViGb7Qs6HdCb8g) 提取码: ddab

![](media/HiSparkguide.png)

#### 配网

使用openharmony手机启动FA应用并输入wifi帐号密码生成二维码。

![](media/setwifi.png)

启动后听到“开始配网”关键词时将二维码对准摄像头。

![](media/QR.png)

#### 车牌识别

听到“配网成功后”将摄像头对准车牌

![](media/licensePlate.png)

在串口中输入1拍照、输入2将照片提交至百度云进行识别，最终返回识别结果。

![](media/getLicensePlate.png)

## 样例联动

本样例由可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)