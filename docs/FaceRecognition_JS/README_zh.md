## [【OpenHarmony样例】基于RK3399开发板开发的智能门禁人脸识别(JS)样例](../../docs/FaceRecognition_JS)

### 一. 概述

本样例是基于RK3399开发板，使用OpenHarmony3.0-LTS开发的应用。通过定时获取摄像头数据，实现人脸识别比对等功能。.

#### 1. 应用运行效果图：

![闸机预览界面](screenshots/人脸识别.jpg)

#### 2. 智能闸机使用示意图

![闸机使用示意图](screenshots/shiyitu.png)

如上图所示，用户通过摄像头区域时，闸机应用通过获取摄像头数据捕捉到人脸数据，进行人脸位置区域、角度亮度及人脸特征计算和比对，返回识别结果给用户。

### 二. 快速上手

### 应用开发

#### 1. 硬件环境准备

* 预装windows系统的PC机
* 扬帆RK3399E开发板
* 开发板专用12V电源适配器
* USB公对公数据连接线
* USB外接摄像头
* HDMI显示器

#### 2. 开发环境准备

* [下载DevEco Studio 3.0 Beta1及以上版本](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/deveco-studio-user-guide-for-openharmony.md)
* [配置OpenHarmonySDK](https://docs.openharmony.cn/pages/00070502/)
* [创建OpenHarmony工程](https://docs.openharmony.cn/pages/0007050300/)
* [配置OpenHarmony应用签名信息](https://docs.openharmony.cn/pages/00070504/)
* [安装运行OpenHarmony应用](https://docs.openharmony.cn/pages/00070505/)

#### 3. 功能开发

* **搭建预览UI页面**

**修改index.html文件**

```
<!--系统camera组件-->
<camera id="CameraId" style="width : {{ previewAreaWidth }} px;
        height : {{ previewAreaHeight }} px;"></camera>
```

添加系统组件camera，宽高根据屏幕规格进行计算，注意宽高比例为3：4

* **实现人脸识别业务功能**

**修改index.js文件**

引入人脸识别库

```
import seetaface from '@ohos.napi_seetaface';
```

在应用初始化时清空已注册的人脸特征

```
seetaface.ClearFaceDatabase()
```

注册人脸特征，imgPath为注册图片路径，返回值id>=0代表注册成功

```
var id = seetaface.RegisterImage(imgPath)
```

开启定时器调用camera的takePhoto函数进行摄像头数据抓取识别，通过回调函数success和fail进行成功和失败处理

```
camera.takePhoto({
    quality: '',
    success: (res) => { // 识别成功
        log('LABEL 9527 takePhoto success -- ' + JSON.stringify(res))
        resolve(undefined)
    },
    fail: (err) => { // 识别失败，err.errorcode为错误码
        log('LABEL 9527 takePhoto fail -- ' + JSON.stringify(err))
        reject(parseInt(err.errorcode))
    }
})
```

注意：1.由于外接屏幕可能不支持触摸交互，导致无法手动启动应用，可以使用以下命令启动

```shell
hdc_std shell aa start -d l -a 应用包名.MainAbility -b 应用包名
```

2.应用覆盖安装导致相机预览失败，需要重启开发板

```shell
hdc_std shell reboot 或 hdc_std target boot
```

3.由于人脸识别底层业务集成在系统camera组件中，导致camera拍照功能（takePhoto）无法存储图片文件，具体业务请查看系统底层人脸识别开发模块

### 系统镜像编译及烧录

#### ***Linux编译服务器基础环境准备***

开发基础环境由windows 工作台和Linux 编译服务器组成。windows 工作台可以通过samba 服务或ssh 方式访问Linux编译服务器。其中windows
工作台用来烧录和代码编辑，Linux编译服务器用来编译OpenHarmony代码，为了简化步骤，Linux编译服务器推荐安装Ubuntu20.04。

##### **1 安装和配置Python**

- 打开Linux终端。

- 输入如下[命令](https://www.elecfans.com/tags/命令/)，查看python版本号，需要使用python3.7以上版本,否则参考 系统基础环境搭建。

  ```
  python3 --version;
  ```

- 安装并升级Python包管理工具（p[ip3](https://www.elecfans.com/tags/IP3/)）。

  ```
  sudo apt-get install python3-setuptools python3-pip -y
  sudo pip3 install --upgrade pip
  ```

##### **2 安装LLVM**

- 下载LLVM工具。

- 解压LLVM安装包至~/llvm路径下。

  ```
  tar -zxvf llvm.tar -C ~/
  ```

- 设置环境变量。

  ```
  vim ~/.bashrc
  ```

  将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

  ```
  export PATH=~/llvm/bin:$PATH
  ```

- 使环境变量生效。

  ```
  source ~/.bashrc
  ```

-

    1. &lt;font size=&quot;4&quot;&gt;source ~/.bashrc&lt;/font&gt;

##### **3 安装hc-gen**

- 打开Linux编译服务器终端。

- 下载hc-gen工具。点击下载地址下载。

- 解压hc-gen安装包到Linux服务器~/hc-gen路径下。

  ```
  tar -xvf hc-gen-0.65-linux.tar -C
  ```

- 设置环境变量。

  ```
  vim ~/.bashrc
  ```

  将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

  ```
  export PATH=~/hc-gen:$PATH
  ```

- 使环境变量生效。

  ```
  source ~/.bashrc
  ```

##### **4 安装编译依赖基础软件（仅Ubuntu 20+需要）**

```
sudo apt-get install build-essential
sudo apt-get install gcc
sudo apt-get install g++
sudo apt-get install make 
sudo apt-get install zlib
sudo apt-get install libffi-dev
```

##### **5 源码下载&&编译准备**

环境搭完后，就该准备代码了。

- 下载repo

```
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
```

- 下载OpenHarmony源码和编译

1. 代码下载

   ```
   ssh方式：repo init -u ssh://git@gitee.com/openharmony-is/manifest.git -b master -m devboard_rk3399.xml --no-repo-verify
   http方式：repo init -u  https://gitee.com/openharmony-is/manifest.git -b master -m devboard_rk3399.xml --no-repo-verify
   repo sync -c
   repo forall -c 'git lfs pull'
   bash build/prebuilts_download.sh
   ```

2. 打patch

   ```
   bash device/rockchip/product/patch.sh
   ```

3. 编译

   ```
   ./build.sh --product-name rk3399 --ccache
   ```

##### **6 编译结果**

编译产生boot.img在目录 /out/KERNEL_OBJ/kernel/src_tmp/linux-4.19 中

编译产生vender.img、system.img 和userdata.img 在目录out/rk3399/packages/phone/images 中

#### ***烧录***

* 系统环境

 window7/windows10

- 驱动安装包

  瑞芯微软件助手安装包，需解压安装

- 烧录工具

 瑞芯微开发工具

![输入图片说明](screenshots/image.png)

- 烧录镜像

  debian_update.img

- 烧录前准备

  拿到开发板后，第一次烧录OpenHarmony系统前，要先进行整体擦除后，对系统分区，再烧录镜像

- 擦除flash

  点击升级固件按钮

  ![输入图片说明](screenshots/%E6%88%AA%E5%9B%BE2.PNG)

  选择固件

  ![输入图片说明](screenshots/%E6%88%AA%E5%9B%BE3.PNG)

擦除flash前，要先使板子进入烧录模式，方法如下：

板子在烧录OpenHarmony系统前，如果有其他系统（如安卓系统），上电后会显示发现一个ADB设备

![输入图片说明](screenshots/%E6%88%AA%E5%9B%BE4.PNG)

先长按uboot按钮，再按一次reset按钮后松开，等待板子显示“发现一个LOADER设备”，板子进入烧录模式

![输入图片说明](screenshots/%E6%88%AA%E5%9B%BE6.PNG)

 点击擦除flash按钮，开始擦除

 擦除完成后，界面显示擦除成功

- 第一次烧录

 第一次烧录OpenHarmony系统，要先加载全部镜像

 在空白处点击右键，选择导入配置，选择ohos.cfg

![输入图片说明](screenshots/%E6%88%AA%E5%9B%BE7.PNG)

 导入配置表后如下图所示

![输入图片说明](screenshots/%E6%88%AA%E5%9B%BE8.PNG)

* 烧录OpenHarmony镜像

 烧录OpenHarmony镜像，只需选中boot、vendor、system、userdata，其他分区不选中
