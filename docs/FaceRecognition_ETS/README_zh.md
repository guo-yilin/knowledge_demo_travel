# [智能门禁人脸识别(eTS)应用开发样例](../../FA/FaceRecognition)

### 样例简介

[人脸识别应用](../../FA/FaceRecognition)是基于OpenHarmony 3.2 Beta标准系统上开发的eTS应用，利用[NAPI组件](../FaceRecognition_NAPI/README.md)调用Seetaface2三方库实现人脸识别能力，主要功能有摄像头拍照、人脸模型录入、人脸框选和人脸识别。

#### 运行效果

![样例效果](./media/index.png)

#### 样例原理

本demo通过NAPI组件调用SeetaFace2接口实现人脸录入、框选和识别功能。

![原理图](./media/principle.png)

#### 工程版本

+ 系统版本/API版本：OpenHarmony 3.2 Beta / SDK API 8
+ IDE版本：DevEco Studio 3.0 Beta3

### 快速上手

#### 准备硬件环境

+ [参考快速入门的环境搭建章节完成环境搭建](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Readme-CN.md)
+ 润和DAYU200开发套件

#### 准备开发环境

+ 安装最新版[DevEco Studio](https://gitee.com/link?target=https%3A%2F%2Fdeveloper.harmonyos.com%2Fcn%2Fdevelop%2Fdeveco-studio%23download_beta_openharmony)。
+ 请参考[配置OpenHarmony SDK](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)，完成**DevEco Studio**的安装和开发环境配置。

#### 准备工程

##### 工程下载

项目地址：https://gitee.com/openharmony-sig/knowledge_demo_travel/tree/master/FA/FaceRecognition

```shell
git clone https://gitee.com/openharmony-sig/knowledge_demo_travel.git --depth=1
```

##### 工程导入

+ DevEco Studio导入本工程;

  打开DevEco Studio,点击File->Open->下载路径/FA/FaceRecognition

#### 编译

+ 点击**File > Project Structure** > **Project > Signing Configs**界面勾选“**Automatically generate signing**”，等待自动签名完成即可，点击“**OK**”。如下图所示：![运行](./media/signed.png)

+ 点击Build->Build Hap/APPs 编译，编译成功生成entry-default-signed.hap

![编译](./media/build.png)

#### 烧录/安装

+ 识别到设备后点击，或使用默认快捷键Shift+F10（macOS为Control+R)运行应用。

![img](./media/install.png)

+ [安装应用](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/installing-openharmony-app.md) 如果IDE没有识别到设备就需要通过命令安装，如下

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```shell
  hdc_std install -r path\entry-default-signed.hap//安装的hap包需为xxx-signed.hap，即安装携带签名信息的hap包。
  ```

#### 操作体验

##### 导入图片

+ 应用安装完成后，下载[导入脚本](https://pan.baidu.com/s/10ExJ5xIcH2otZVLLdu74cw?pwd=lzb9 )双击bat脚本一键导入。
  ![bat](./media/bat.png)
+ 或使用hdc_std file send local remote进行发送图片,local为本地图片路径，remote为应用目录即/data/app/el2/100/base/com.teamx.facerecognition/haps/entry/face。

![send_images.png](./media/send_images.png)

##### 录入人脸

![recognize.gif](./media/setFaceData.gif)

##### 人脸框选与识别

![setFaceData](./media/recognize.gif)

### 参考资料

+ [OpenHarmony 基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
+ [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/application-dev/reference/apis/Readme-CN.md)
+ [应用开发导读](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/application-dev-guide.md/)
