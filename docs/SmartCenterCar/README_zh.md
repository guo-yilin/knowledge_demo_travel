# [鸿蒙智联汽车【1.0】](../../FA/SmartCenterCar)

## 应用场景：

- 智慧出行。

智能汽车是集环境感知、规划决策、多等级辅助驾驶等功能于一体的智能网联综合系统，它集中运用了计算机、现代传感、信息融合、通讯、人工智能及自动控制等技术，是典型的高新技术综合体。简单的说，智能汽车的出现将逐步放松车、手、眼，让开车，用车变得简单。这样的产品对有本儿不敢上路的人来说或许是大大的福音。

在北方冬天有点冷，这个时候，去车里，温度很低，给人一种不舒服的感觉，那么有没有一种可能，就是可以通过手机App，实现对车内的一些情况的监测，答案是有的，今天做的这个App，就是这样一个App。

我要实现的功能主要有

- 用户可以解锁任何车门，
- 检查电池状态，
- 控制空调温度，
- 检查轮胎的气压。

## 样例效果

![智联汽车演示_](res/智联汽车演示.gif)





## 目录结构

```js
│  app.ets
│
├─common
│  └─images
│          Car.svg
│          cars.jpg
│          door_lock.svg
│          door_unlock.svg
│          tab-home-Select.png
│          tab-home-Unchecked.png
│          tab-my-Select.png
│          tab-my-Unchecked.png
│          tab-news-Select.png
│          tab-news-Unchecked.png
│
└─pages
        battery_page.ets 
        door_lock_page.ets
        index.ets
        temp_page.ets
        tyre_page.ets
        tyre_psi_card.ets

```




### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，链接。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta4。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程,SmartCenterCar

4）OpenHarmony应用运行在真机设备上，需要进行签名，[签名方法](https://docs.openharmony.cn/pages/v3.1/zh-cn/application-dev/security/hapsigntool-guidelines.md/)。
