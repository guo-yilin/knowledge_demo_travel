# [天气预报](../../FA/WeatherForeCast)



## 简介

天气预报，目前实现的功能：



- 预报北京市实时天气预报
- 北京市未来七天的天气



## 样例效果








先来看一下

## 目录结构

```js
├── config.json
├── ets
│   └── MainAbility
│       ├── app.ets
│       ├── common
│       │   └── RealtimeWeather.ets
│       ├── data
│       │   ├── get_test.ets
│       │   └── get_week_test.ets
│       ├── model
│       │   ├── daily.ets
│       │   ├── now.ets
│       │   └── weatherModel.ets
│       └── pages
│           └── Main.ets
└── resources
    ├── base
    │   ├── element
    │   │   ├── color.json
    │   │   └── string.json
    │   └── media
    │       └── icon.png
    └── rawfile


```








### 安装部署

##### 1.代码编译运行步骤

1）[下载此项目](../../FA/WeatherForeCast/README.md)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta4。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程,WeatherForeCast



4）OpenHarmony应用运行在真机设备上，需要进行签名，[签名方法](https://docs.openharmony.cn/pages/v3.1/zh-cn/application-dev/security/hapsigntool-guidelines.md/)。
