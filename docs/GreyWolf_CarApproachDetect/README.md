# [智能停车系统之车辆出入检测器](../../dev/team_x/GreyWolf_CarApproachDetect)

## 一、设备展示

### 1. 样例效果

[车辆出入监测设备](../../dev/team_x/GreyWolf_CarApproachDetect)基于OpenHarmony操作系统，设备控制使用欧智通BES2600开发板，系统版本为OpenHarmony3.1 Release，传感器模块采用HC_SR04超声波测距模块。当有车辆靠近较远范围时，开发板蓝色LED灯闪烁，此时认为车辆靠近。当有车辆靠近较近范围时，开发板蓝色LED灯常亮，此时认为车辆正在等待驶入或驶出。当指定范围内没有障碍物时，开发板蓝色LED灯熄灭，此时认为无车辆。由此实现门岗车辆驶入驶出监测。

![index](media/car_approach_detect_practice.gif)

配合门闸、车牌识别设备、车辆管理终端设备可对停车场进行车辆出入管理的功能。设备初次上电时，红色LED灯以高频率闪烁，此时设备处于待配网状态。当使用配套手机FA配网后，红色LED灯以较低频率闪烁，此时设备处于连接到路由器状态。当红色LED灯常亮时，代表设备本地组网模块开始工作。

![records](media/car_approach_detect_net_config.gif)



### 2. 涉及OpenHarmony技术特性

- HDF、网络通信

### 3. 支持OpenHarmony版本

- OpenHarmony 3.1 Release

### 4. 支持开发板

- 欧智通BES2600开发板

## 二、快速上手

#### 1.轻量型设备环境准备

  欧智通BES2600开发板：

- [BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/bes2600_hello_world)

#### 2.项目下载和导入

1）git下载

代码地址：[链接](../../dev/team_x/GreyWolf_CarApproachDetect)

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_travel.git
git clone https://gitee.com/openharmony-sig/knowledge_demo_smart_home.git
```

2）项目导入

- 产品代码导入

  ```
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_CarApproachDetect/car_detect ~/OpenHarmony_3.1_Release/vendor/team_x
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_CarApproachDetect/common ~/OpenHarmony_3.1_Release/vendor/team_x
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_CarApproachDetect/v200zr-evb-t1.hcs ~/OpenHarmony_3.1_Release/device/board/fnlink/shields/v200zr-evb-t1/v200zr-evb-t1.hcs
  ```

- 系统源码补充和修改

  - kernel/liteos_m 修改

    - 下载patch文件 [bes_kernle.patch](https://gitee.com/kenio_zhang/kernel_liteos_m/commit/d6e85103f49a12f13463dfd5b2bfd8274728d90a.diff)

      1.点击上述链接进入浏览器，将该网页中内容全部复制。

      2.本地创建一个名为bes_kernle.patch文件，并将已经复制的内容粘贴到该文件中。

    - 将patch文件打入源码

      1.将bes_kernle.patch文件复制到~/OpenHarmony_3.1_Beta/kernel/liteos_m目录下。

      2.执行如下命令将patch文件打入系统源码。

      ```
      cd ~/OpenHarmony_3.1_Beta/kernel/liteos_m
      patch -p1 < bes_kernle.patch
      ```


  - device/soc/bestechnic 修改

    - 下载patch文件 [bes_device.patch](https://gitee.com/kenio_zhang/device_soc_bestechnic/commit/55fbd420d5612773c47948d715ccddbff98379be.diff)。

      1.点击上述链接进入浏览器，将该网页中内容全部复制。

      2.本地创建一个名为bes_device.patch文件，并将已经复制的内容粘贴到该文件中。

    - 将patch文件打入源码

      1.将bes_kernle.patch文件复制到~/OpenHarmony_3.1_Beta/device/soc/bestechnic目录下。

      2.执行如下命令将patch文件打入系统源码。

      ```
      cd ~/OpenHarmony_3.1_Beta/device/soc/bestechnic
      patch -p1 < bes_device.patch
      ```

  - flash_size配置修改

    根据开发板的硬件版本不同，还需更改配置文件中flash_size的值。具体路径为/device/board/fnlink/v200zr/liteos_m/config.gni。其中v1.0版本对应16，2.0版本对应32。硬件版本打印在了开发板正面。

    ```
    diff --git a/v200zr/liteos_m/config.gni b/v200zr/liteos_m/config.gni
    index b288101..6489dd2 100644
    --- a/v200zr/liteos_m/config.gni
    +++ b/v200zr/liteos_m/config.gni
    @@ -47,7 +47,7 @@ if (product_path != "") {
       bsp_bin_list = product_conf.bsp_bin_list
       pack_burn = product_conf.pack_burn
     }
    -flash_size = 16
    +flash_size = 32
    ```

#### 3.编译和烧录

参考[BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/bes2600_hello_world)完成项目的编译和烧录。

## 三、关键代码解读

#### 1.目录结构

```
.
├─vendor
   └─team_x
       └─car_detect
           ├─demo_car_detect
           │  ├─external_device //基于HDF的外设驱动
           │  │   ├─key.c // 按键
           |  |   ├─led.c // LED灯
           |  |   └─hcsr04_control.c // HC_SR04模块
           │  ├─local_net // 本地组网通信
           │  │   ├─local_net_communication.c // 本地组网通信协议
           │  │   ├─local_net_udp.c // 本地组网udp通信
           │  │   ├─local_net_dlist.c // 链表存储
           │  │   └─local_net_utils.c // 工具类接口（SHA256等）
           │  ├─iot_main.c// 车辆驶入驶出监测主逻辑
           └─config.json等配置文件及文件夹 设备通用
```

#### 2.关键代码

创建车辆驶入驶出监测任务和LocalNet本地网络通信启动任务。

![](media/iot_main_start_code.png)

车辆检测任务中，按键、LED指示灯、HCSR04模块初始化后进入任务循环。循环中读取HCSR04超声波传感器模块的测量结果，按照较近范围（50cm）、较远范围（60cm）、不在区域内（60cm外）的标准刷新区域内车辆的状态。不断获取按键状态，判断用户是否发起清空路由信息操作。

![](media/car_detect_task_init_and_detect.png)

当区域内车辆状态发生变化时，按照较近范围蓝色LED灯常亮、较远范围蓝色LED灯闪烁、不在区域内蓝色LED灯熄灭的规则设置LED灯状态，指示区域内车辆驶入驶出状态。当LocalNet本地通信模块启动OK后，将变化后的车辆监测状态进行发布。

![](media/car_detect_task_localnet_send.png)

在本地通信模块启动任务中，首先读取路由信息，当未读取到路由信息时，启动softap配网并设置红色LED灯快速闪烁指示当前状态，否则跳过该步骤。网络配置完成或路由信息读取成功后，连接路由、重新写入路由信息、设置红色LED灯较慢速率闪烁指示当前状态。

![](media/local_net_task_config_and_connect_wifi.png)

配置设备自身信息，并对LocalNet本地通信息模块进行初始化、设置红色LED灯常亮指示当前状态。

![](media/local_net_self_info_set_and_init.png)

## 四、样例联动

本样例可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)

## 五、参考链接

- [BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/bes2600_hello_world)