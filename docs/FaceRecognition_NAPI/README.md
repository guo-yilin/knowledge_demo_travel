## 人脸识别样例

### 样例简介

本样例是 基于OpenHarmony 3.1 Release 版本开发的NAPI接口，与[人脸识别应用](../FaceRecognition_ETS/README_zh.md)共同组成人脸识别样例，实现了人脸头像的框选，人脸注册以及人脸识别等功能。

#### 运行效果

![运行效果图](media/operation1.gif)

#### 样例原理

​       ![Operation effect](media/operation_effect.png)

如上图所示，用户可以自行设置人脸模型，将人脸模型设置完后，可以通过该人脸不同的头像进行识别，识别成功后返回对应的人脸模型的名字。

#### 工程版本

- 系统版本/API版本：OpenHarmony 3.1 release
- 工具链版本 ：gcc-linaro-7.5.0-arm-linux-gnueabi

### 快速上手

#### 准备硬件环境

 润和RK3568开发板 .

#### 准备开发环境

本样例是基于OpenHarmony 3.1 Release 版本开发的标准系统应用，使用的开发板是rk3568开发板，开发环境搭建也可以参照[润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)。

#### 准备工程

通过环境搭建环节，我们就已经下载并配置好了OpenHarmony SDK的代码，接下来我们需要获取本样例相关的其他代码。

##### 获取应用相关代码

通过以下指令，我们可以获取到应用相关的代码：

```shell
git clone git@gitee.com:openharmony-sig/knowledge_demo_travel.git --depth=1
```

##### 获取opencv代码

OPencv是一个功能非常强大的开源计算机视觉库。而且此已经由第三方库开发小组移植到了openharmony中，后期还会将此库合入到主仓，在此库上主仓之前，我们只需要以下几个步骤就可以实现opencv的移植使用.

- 通过以下命令下载已经移植好的opencv：

  ```shell
  git clone git@gitee.com:zhong-luping/ohos_opencv.git
  ```

- 将opencv拷贝到openharmony目录的third_party下：

  ```shell
  cp -raf opencv ~/openharmony/third_party/
  ```

- 适当裁剪编译选项：

  打开opencv目录下的BUILD.gn，将其中不需要用到的依赖选项注释即可。如下图：

  ​![core_build](media/opencv_core_gn.png)

- 添加依赖子系统的part_name，使得最后编译出的库能自动拷贝到系统文件中：

  此项目中我们新建了一个SeetaFaceApp的子系统，该子系统中命名part_name为SeetafaceApi，所以我们需要在对应模块中的BUILD.gn中加上part_name="SeetafaceApi"(各模块在Opencv/module目录下)

  以module/core为例

  ```gn
  ohos_shared_library("opencv_core"){
      sources = [ ... ]
      configs = [  ... ]
      deps = [ ... ]

      part_name = "SeetafaceApi"
  }
  ```

##### 获取SeetaFace2代码

SeetaFace2 是中科视拓开源的第二代人脸识别库。包括了搭建一套全自动人脸识别系统所需的三个核心模块，即：人脸检测模块 FaceDetector、面部关键点定位模块 FaceLandmarker 以及人脸特征提取与比对模块 FaceRecognizer。

- 拷贝SeetaFace2代码

  SeetaFace2的代码在应用端代码的dev/third_party下，直接将该代码复制到OpenHarmony的third_party下：

  ```shell
  cp -arf ~/knowledge_demo_travel/dev/third_party/SeetaFace2 ~/openharmony/third_party/
  ```

##### 获取SeeatFaceNapi代码

将SeeatFaceNapi代码拷贝到OpenHarmony目录下

```shell
cp -arf ~/knowledge_demo_travel/dev/team_x/FaceRecognition/seetaface ~/openharmony/
```

##### 添加子系统

- 打开build/subsystem_config.json，添加新的子系统（该子系统定义在seetaface下的ohos.build中）：

  ```js
  "SeetafaceApp": {
      "path": "seetaface",
      "name": "SeetafaceApp"
  }
  ```

- 产品中添加新的子系统信息：

  打开文件productdefine/common/products/rk3568.json，并在文件最后添加：

  ```js
  "SeetafaceApp:SeetafaceApi":{}
  ```

#### 编译

```shell
#进入源码根目录，执行如下命令进行版本编译
./build.sh --product-name rk3568

#编译完成后会有如下显示
=====build rk3568 successful.
```

#### 烧录/安装

烧录方式参照[润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/rk3568_helloworld/README.md#%E9%95%9C%E5%83%8F%E7%83%A7%E5%BD%95)

#### 操作体验

- 注册人脸

  &nbsp;![注册人脸](media/operation1.gif)

- 框选/识别人脸

  &nbsp;![识别人脸](media/operation2.gif)

### 参考资料

- [OpenHarmony标准系统入门](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Release/zh-cn/device-dev/quick-start/quickstart-ide-standard-overview.md).
- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/rk3568_helloworld/README.md).
- [Rk3568开发板介绍](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Release/zh-cn/device-dev/quick-start/quickstart-ide-standard-board-introduction-rk3568.md).
- [知识体系](https://gitee.com/openharmony-sig/knowledge).
