# [智能停车配网FA](../../FA/GreyWolf_NetConfig)

### 简介

[智能停车配网FA](../../FA/GreyWolf_NetConfig)用于智能停车场中除停车场后台管理的其他设备配网，可支持softAP配网和二维码配网，更多智能停车场请 [点击链接 ](../GreyWolf_SmartParking/README.md)进行了解。

### softAP配网

通过手机连入车辆出入检测器和门闸控制器的发出的热点来进行配网，通过socket将配网信息（ssid和password）和设备分组信息发送至系统中，实现配网功能和设备分组功能。

![softap](media/softAP.png)

### 二维码配网

如车牌识别器拥有牌照能力，那么可通过将配网信息和分组信息生成二维码让车牌识别器进行扫描识别，从而实现配网和分组功能。

![qrcode](media/qrcode.png)

