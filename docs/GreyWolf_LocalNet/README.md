# 本地组网通信模块

本地组网通信模块是基于发布订阅机制的自组网通信模块。软件结构分为以下部分：

- 协议层：负责处理发布、订阅、设备在线等协议相关的逻辑处理。
- 网络层：基于UDP通信构建的本地网络，可实现网络内单播和广播通信。
- 系统适配层：屏蔽不同系统间metux、thread、queue、delay等接口的差异。

<img src="media/software_struct.png" alt="image-20220412103941620"  />

## 一、协议



### 1. 广播

<img src="media/localNetBroadcast.png" alt="image-20220411171533446" style="zoom:50%;" />

设备自启动本地组网通信模块开始会以8~16s为周期发送广播包，广播包内容为设备本身的信息。广播用于表明设备在线、传达自身能力、传达自身需求数据，因此需要广播自身数据（名字、类型、id、分组、优先级）、订阅哪些数据、我能发布什么（能输出什么），广播协议格式如下：

```
{
	"messageType" : "broadcast",
	"message" : {
        "device" : {
            "name":"设备名 用于展示方便，开发者查看"
            "type" : "设备类型",
            "id" : "唯一标识符 1. type + '_' + group + '_' + mac经过SHA256加密组成唯一id",  
            "group" : "A_ENTRANCE 分组 A/B/C/D + '_' + ENTRANCE/EXIT 组成", 
            "priority" : 50 // 优先级 L0 0-99 L1 100-199 L2 200-299 发布时判断是否由自己发布
        },
        "subscribe" : [], // 需要订阅的消息
        "publish" : [] // 发布能力
    }
}
```

### 2. 单播

<img src="media/localNetUnicast.png" style="zoom:50%;" />

当设备对外发送能力，或对接收到的别人发布的能力进行回复时会使用单播。单播协议格式如下：

- 能力发布

```
{
	"messageType" : "command",
	"messageId" : 0, // 0~255
	"message" : {
		"publish" : "发布的消息类型",
		"params" :{
			"key" : "value"
		}
	}
}
```

- 命令回复

```
{
	"messageType" : "response",
	"messageId" : 0, // 0~255
	"message" : {
	    "id" : "xxxxxxxxxxx",  // 64位设备唯一识别Id
	    "result" : "ok", // 能力发布结果
	    "state" : 200  // 通信结果 
	}
}
```

### 3. 订阅/发布机制

<img src="media/localNetSubscribeAndPublish.png" alt="image-20220411173254714" style="zoom:40%;" />

在广播包中会包含设备订阅和发布的消息类型。假设设备A、B进行组网通信。在A设备收到B设备的广播包后，解析出B设备订阅了msg类型的消息。而A设备刚好具备发布这种消息的类型，那么当A设备发布msg类型的消息时，就会在待发送目标中找到B设备，并以单播的形式将该消息发送给设备B。

### 4. 优先级

在多个设备组网通信的情境下，设备发布的消息的类型有可能重复。此时，仅优先级高的设备才拥有该消息类型的发布权限。建议L0设备优先级：0~99，L1设备优先级：100~199，L2设备优先级：200~299。

### 5. 分组通信

L2设备可以接收所有分组设备的消息。非L2设备在配网阶段会配置一个分组，只有同分组的设备之间才可以通信。

## 二、本地组网通信模块适配

用户需基于自身设备运行的OS API来实现模块中OS相关的接口，否则模块将无法工作。

需适配的API如下：

|                           函数声明                           |     描述     |
| :----------------------------------------------------------: | :----------: |
|       void     LocalNetDelayMs (  uint32_t    ms  ) ;        |   线程延时   |
| **LocalNetThreadHandle**     LocalNetThreadCreate (  **LocalNetThreadAttr***    threadAttr ,   LocalNetThreadFun**    threadFun ,  void*    arg  ) ; |   线程创建   |
| int8_t     LocalNetThreadExit (  **LocalNetThreadHandle***      threadHandle ) ; |   线程退出   |
|  **LocalNetMutexHandle**     LocalNetMetuxCreate ( void ) ;  |  互斥锁创建  |
| int8_t     LocalNetMetuxDelete (  **LocalNetMutexHandle**     metuxHandle  ) ; |  互斥锁销毁  |
| int8_t     LocalNetMetuxGet (  **LocalNetMutexHandle**     metuxHandle  ) ; |  互斥锁获取  |
| int8_t     LocalNetMetuxSet (  **LocalNetMutexHandle**     metuxHandle  ) ; |  互斥锁释放  |
| **LocalNetQueueHandle**     LocalNetQueueCreate (  uint32_t     msg_count ,  uint32_t     msg_size  ) ; | 消息队列创建 |
| int8_t     LocalNetQueueDelete (  **LocalNetQueueHandle**     queueHandle  ) ; | 消息队列销毁 |
| int8_t     LocalNetQueueSend (  **LocalNetQueueHandle**     queueHandle ,  const     void     *msg_addr  ) ; | 消息队列发送 |
| int8_t     LocalNetQueueRecv (  **LocalNetQueueHandle**     queueHandle ,  void     *msg_addr  ) ; | 消息队列接收 |

## 三、本地组网通信API使用

本地组网通信模块共有5个API供用户按需使用，函数声明整理如下：

|                             API                              |           描述           |
| :----------------------------------------------------------: | :----------------------: |
|  int8_t LocalNetSelfInfoSet(NetBroadcastPara_t *selfInfo);   |       设备信息设置       |
| int8_t LocalNetMsgRecvCbReg(int8_t (*recvCb)(const char *msg)); |   订阅消息接收回调注册   |
| int8_t LocalNetDevListNoticeCbReg(int8_t (*devListNoticeCb)(Node_t *devList)); | 在现设备列表刷新回调注册 |
|                  int8_t LocalNetInit(void);                  |  本地组网通信模块初始化  |
| int8_t LocalNetMsgSend(const char *publish, const char *params); |       发布消息发送       |
|                 int8_t LocalNetDeinit(void);                 | 本地组网通信模块逆初始化 |

用户在使用本地组网通信模块要先确保设备连接到本地网络。首先，要设置设备信息；其次，如果设备有获取其他在线设备的需求，则调用注册函数，将在线设备列表刷新处理函数注册到模块中；然后，如果设备有需要订阅的消息，则调用注册函数，将接收处理函数注册到模块中；然后，调用模块初始化函数，则本地组网通信模块开始工作；最后，当设备需要发布消息时，则调用发送函数进行发布。

![image-20220413115951192](media/LocalNetUseCase.png)

