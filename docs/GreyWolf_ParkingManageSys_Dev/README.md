# [智能停车场车辆管理中控台NAPI接口的开发及其应用](../../dev/team_x/GreyWolf_ParkingManageSys_Dev)

## 简介

###  NAPI简介

 NAPI（Native API）是 OpenHarmony 标准系统的一种JS API实现机制，适合封装IO、CPU密集型、OS底层等能力并对外暴露JS接口，通过NAPI可以实现JS与C/C++代码互相访问。

![image-20220328170045472](media/image-20220328170045472.png)

详情可访问NAPI组件开源代码仓：https://gitee.com/openharmony/ace_napi。

### 涉及OpenHarmony技术特性

- NAPI

### 支持OpenHarmony版本

- OpenHarmony 3.1 Release

### 支持开发板

- 润和RK3568开发板

### 标准设备环境准备

润和RK3568开发板：

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld);

## 快速上手

### 创建NAPI扩展库

#### 获取OpenHarmony源码

OpenHarmony标准系统的NAPI扩展库，目前不支持脱离系统源码单独开发构建，不支持随应用发布，需要编译时预置到系统中。

当前适配OpenHarmony 3.1 Release，通过repo下载此发布版代码：

``` shell
repo init -u https://gitee.com/openharmony/manifest.git -b refs/tags/OpenHarmony-v3.1-Release --no-repo-verify
repo sync –c
repo forall -c 'git lfs pull'
```

#### 初始化NAPI扩展库

OpenHarmony系统功能按系统、子系统、组件逐级展开，可根据业务需求来裁剪编译的组件，具体可参考[标准系统编译构建指导文档](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/device-dev/subsystems/subsys-build-standard-large.md)。

1、新增子系统 [“parkingsubsys”](../../dev/team_x/GreyWolf_ParkingManageSys_Dev)

- 在根目录下创建parkingsubsys目录（子系统可创建在OpenHarmony源码目录下任意位置）。

- 修改子系统配置文件build/subsystem_config.json，添加parkingsubsys子系统配置，包含名称和路径。如图：

![image-20220328170706484](media/image-20220328170706484.png)

2、新增parkingsubsys组件

- 在parkingsubsys目录下创建ohos.build文件，定义子系统的组件，如图：

  ![image-20220328171335723](media/image-20220328171335723.png)

  - 在parkingsubsys目录下创建一个子目录parking，作为组件的代码目录。如图：

  ![image-20220328171137307](media/image-20220328171137307.png)

2.2 初始化NAPI扩展库

- 在组件目录下创建一个子目录parikingapi，作为NAPI扩展库的代码目录。

- 在parikingapi目录下创建代码文件parikingapi.cpp。

- 在parikingapi目录下创建BUILD.gn文件，编写构建配置。如图：

![image-20220328171854757](media/image-20220328171854757.png)

- 初始化后，parikingapi子系统的目录结构，如图：

  ![image-20220328172346500](media/image-20220328172346500.png)

2.3 **将组件添加到产品定义中**

产品定义文件位置：productdefine/common/products/${product-name}.json

在产品配置文件中添加 "subsystemA:partA"，表示该产品中会编译并打包partA到版本中

本文示例在rk3568开发板验证，对应的产品定义文件为：productdefine/common/products/rk3568.json，通过添加配置“parikingapi:parikingapi ”将parikingapi组件添加到版本中。如图：

![image-20220328172601364](media/image-20220328172601364.png)

### NAPI接口开发和烧录

已有接口定义如下：

![image-20220328172601364](media/napi.png)

详细接口定义请查阅： [@ohos.ParkingApi.d.ts](media/@ohos.ParkingApi.d.ts)  

#### 模块注册

- 添加NAPI框架头文件，引入框架提供的方法。

- 定义模块

  ​a、模块名：**nm_modname**，JS应用层通过这个模块名调用。

  ​b、接口注册函数：**nm_register_func****，**在此函数中注册本模块要导出的接口。

- 注册模块，加载动态库时自动调用该方法。如图：

  ![image-20220328172959503](media/image-20220328172959503.png)

#### 接口注册

使用DECLARE_NAPI_FUNCTION、DECLARE_NAPI_PROPERTY、DECLARE_NAPI_STATIC_PROPERTY等定义属性，再通过napi_define_properties赋给exports对象，最后返回exports对象。如图：

![image-20220328173043691](media/image-20220328173043691.png)

#### 构建与烧录

- 先下载安装构建依赖工具（仅需一次），在源码根目录下执行脚本：bash build/prebuilts_download.sh，如图：

![image-20220328182124016](media/image-20220328182124016.png)

- 编译构建对应产品的镜像， rk3568的构建命令： ./build.sh --product-name rk3568 --ccache， 如图

![image-20220328182050662](media/image-20220328182050662.png)

- 编译成功，如图：

  ![image-20220328182212760](media/image-20220328182212760.png)

编译成功后，按照操作系统版本是否变化分为两种场景：

场景一：如果操作系统变化，则需要重新烧录，所以请使用如下镜像：

- rk3568构建生成的内容归档在out/rk3568/目录，镜像输出在 out/rk3568/packages/phone/images/ 目录。如图：

![image-20220328182424173](media/image-20220328182424173.png)

场景二：系统烧录后，如系统版本未变，可直接使用hdc工具将新构建的 out/rk3568/packages/phone/system/lib/module/libparkingapi.z.so 文件复制替换开发板系统中的 /system/lib/module/libparkingapi.z.so 文件，提升验证效率。

![image-20220328182838946](media/image-20220328182838946.png)

#### **替换方式**

打开串口调试工具，打开串口，如图：

![image-20220328183317110](media/image-20220328183317110.png)

串口控制台执行命令添加写权限： “mount -o remount,rw / ”，如图：

![image-20220328183424643](media/image-20220328183424643.png)

通过hdc_std工具发送替换文件：hdc_std file send libparkingapi.z.so /system/lib/module/， 如图：

![image-20220328183640297](media/image-20220328183640297.png)

更多构建相关内容参考：[标准系统编译构建指导](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/device-dev/subsystems/subsys-build-standard-large.md)

## 样例联动

本样例由苍狼战队开发，可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)

## 参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)
