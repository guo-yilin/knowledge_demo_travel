## 语音AI子系统集成

1. 系统版本要求：OpenHarmony 3.1Release， 请确认当前开发板的版本**录音功能、网络功能**是否正常。

2. 解压[data.zip文件](../../dev/team_x/PATEO_CarVoiceAssistant/data.zip)

3. 使用hdc工具将data中的文件发送到OpenHarmony系统中：

   ```bash
   #1. 将动态库和资源文件发送到OpenHarmony系统中
   # 如果提示Read only system;进入OH系统后执行:"mount -o rw,remount /"命令后再发送文件
   hdc_std.exe file send voice_assistant_service.xml /system/profile/
   hdc_std.exe file send libcarvoiceassistant.z.so /system/lib/module/libcarvoiceassistant.z.so
   hdc_std.exe file send libvoiceassistant_service.z.so /system/lib/libvoiceassistant_service.z.so
   hdc_std.exe file send libpocketsphinx.z.so /system/lib/module/libpocketsphinx.z.so
   hdc_std.exe file send libps_vad.z.so /system/lib/module/libps_vad.z.so
   hdc_std.exe file send libvoicecloud.z.so /system/lib/libvoicecloud.z.so
   hdc_std.exe file send voice_assistant_service.cfg /system/etc/init/
   
   #在系统/system/etc/下，创建目录pocketsphinx; 创建目录命令: mkdir /system/etc/pocketsphinx
   hdc_std.exe file send voice_tip.mp3 /system/etc/pocketsphinx/
   hdc_std.exe file send zh.tar /system/etc/pocketsphinx/
   
   #在OpenHarmony系统中解压zh.tar
   tar xvf zh.tar
   
   #确保/system/etc/pocketsphinx/下文件目录结构如下：
   ├── zh
   │   ├── zh
   │   │   ├── feat.params
   │   │   ├── feature_transform
   │   │   ├── mdef
   │   │   ├── means
   │   │   ├── mixture_weights
   │   │   ├── noisedict
   │   │   ├── transition_matrices
   │   │   └── variances
   │   ├── zh_cn.dic
   │   └── zh_cn.lm.bin
   ├── voice_tip.mp3
   
   #重启系统
   ```

