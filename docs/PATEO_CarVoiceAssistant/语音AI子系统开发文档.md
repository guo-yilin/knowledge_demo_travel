## [语音AI子系统开发文档](../../dev/team_x/PATEO_CarVoiceAssistant)

### 1. 子系统介绍

语音子系统作为`miscservices`子系统下一个`part`(`voiceassistant`)集成在系统代码中；包含9个`module`,实现语音唤醒、语义解析、TTS播报、录音等功能；App调用JS接口，通过NPI调用client端代码，client发消息给server端，server端执行具体逻辑。

**代码目录: [语音AI子系统目录](../../dev/team_x/PATEO_CarVoiceAssistant/voiceassistant)**

### 2. 编译代码

1. 准备好OpenHarmony源码编译环境,参考[标准系统编译构建指导](https://docs.openharmony.cn/pages/v3.1/zh-cn/device-dev/subsystems/subsys-build-standard-large.md/)；
2. 执行`prebuild.sh`( [脚本所在目录](../../dev/team_x/PATEO_CarVoiceAssistant/voiceassistant))脚本，将云语音动态库、唤醒语言模型等资源解压到代码目录中；
3. 将`voiceassistant`目录拷贝到系统源码`系统源码目录/base/miscservices/`下；
4. 修改`系统源码目录/productdefine/common/products/DAYU.json`文件，添加`"miscservices:voiceassistant":{},`;
5. 执行编译指令`build.sh --product-name DAYU`

### 3. 开发板集成

集成有两种方式：

1. 将源码编译生成的镜像刷到开发版中；具体参考开发板的刷机教程；

2. 如果您开发板已经安装好OpenHarmony 3.1 Release系统，可以将生成的动态库、资源文件，参考[语音AI子系统集成文档](语音AI子系统集成文档.md)集成。

### 4. 代码架构

1. 代码目录如下：

```bash
├── etc #语音助理SA服务启动配置
├── frameworks #FWK目录
│   ├── manager #管理类，包括录音管理、唤醒管理、TTS播报管理、云语音动态库管理
│   ├── pocketsphinx #开源库pocketsphinx，用于语音识别
│   ├── utils #工具类、日志等
│   ├── vad #开源库VAD检测，用于分割录音中有效音频部分
│   └── voiceclouddll #云语音动态库，用于语义解析、TTS文字转语音
├── interfaces #NAPI模块
│   └── kits
│       └── js
│           ├── declaration #定义ts接口文件
│           └── napi #NAPI实现类
├── profile #语音助理SA服务配置
├── resources #资源文件：语言模型、提示音文件
├── services #SA服务server、client代码
└── test #测试类

```

2. 唤醒、语义识别流程图如下：

![子系统唤醒流程图](./media/子系统唤醒流程图.jpg)

3. module说明：

   `pocketsphinx`:语音唤醒库；

   `ps_vad`:VAD检测库，用于检测有效音频流；

   `voicecloud_dll`:云语音动态库；

   `voice_assistant_service.rc`:语音助理SA服务启动配置，开机后自动启动语音助理服务；

   `voice_assistant_sa_profiles`:语音助理SA服务配置；

   `pocketsphinx_all_source`:语音助理相关资源，包含语言模型、提示音文件；

   `voiceassistant_js`: JS声明文件；

   `voiceassistant_service_group`:包含SA服务动态库和语音助理动态库

   `carvoiceassistant`:语音助理动态库,App层调用引用JS声明文件，自动加载该动态库，JS接口的具体实现代码也在该动态库中；

   `voiceassistant_service`:SA服务动态库，系统启动时加载该动态库启动语音助理SA服务;

   `client_test`:测试程序。

4. 相关类和文件说明：

   NAPI：

   `@ohos.carvoiceassistant.d.ts`:语音助理ts声明文件；

   `VoiceAssistantEventTarget`:client端回调到App的NAPI接口实现；

   `voice_assistant_napi.cpp`:NAPI接口实现；

   framework：

   `AudioRecordManager`:音频录制类，用于录制音频，返回音频流；

   `IWakeUpManager`:唤醒抽象类，包含音频流处理接口，唤醒结果回调接口，开发者可以实现该抽象类接口来替换样例的唤醒引擎；

   `WakeUpManager`: `IWakeUpManager`的具体实现类，使用 WebRtcVad分割音频，pocketsphinx语音识别;

   `TTSManager`:TTS管理类，用于TTS文字转语音(调用云语音接口)，并调用播放器播放；

   `voice_cloud_loader`:云语音动态库加载，提供文字转语音接口和语义解析功能。

   `IVoiceCloudManager`:云语音解析抽象类，包含语义解析、TTS文字转语音接口，开发者可以实现该抽象类接口来替换样例的云语音动态库；

   server端：

   `VoiceAssistantAbilityAgentStub`:负责接收client端发来的消息，并调用相关管理类处理消息；

   `VoiceAssistantAgentService`:SA服务管理类；

   `VoiceAssistantCallbackEventTarget`:client回调管理类，用于client回调的注册、注销、回调处理；

   `VoiceAssistantClientCallbackProxy`:client回调类，用于回调语义解析消息、唤醒状态、识别状态等到client端；

   `VoiceAssistantClientCallbackDeathRecipient`:client回调状态监听类，client断开时注销回调；

   client端：

   `IVoiceAssistantAbilityAgent`:与server通信抽象类；

   `VoiceAssistantAbilityAgentProxy`:实现与server具体通信逻辑；

   `VoiceAssistantClientCallbackStub`:接收server回调，并通过NAPI接口回调到App；

   `VoiceAssistantClientManager`:client端管理类，用于与server端建立连接，发送消息。