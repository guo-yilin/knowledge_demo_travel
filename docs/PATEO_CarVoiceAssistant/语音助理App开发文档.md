## [语音助理App开发文档](../../FA/PATEO_CarVoiceAssistant)

### 1. 介绍

语音助理App主要实现语音唤醒、语音识别、热词注册、文字上屏、车辆控制等功能；使用eTS语言开发。运行此App时，请确认开发板已集成语音子系统服务，语音子系统集成文档链接：[语音AI子系统集成文档](./语音AI子系统集成文档.md)。

**代码目录：[语音助理App目录](../../FA/PATEO_CarVoiceAssistant)**

### 2. 主要功能介绍

1. 引入语音助理声明文件,声明文件:[文件链接](../../dev/team_x/PATEO_CarVoiceAssistant/voiceassistant/interfaces/kits/js/declaration/@ohos.carvoiceassistant.d.ts)

   ```js
   import carvoiceassistant from '@ohos.carvoiceassistant'
   let voiceManager = carvoiceassistant.getManager(); // 获取语音助理管理类
   ```

2. 开启唤醒

   ```js
   voiceManager.enableWakeUp()
   ```

3. 注册热词

   ```js
   voiceManager.registerHotwords(JSON.stringify(hotwords))
   ```

4. 经纬度设置，用于云语音定位地理位置；例如“今天天气怎么样？”语义可以返回设置的经纬度地区的天气信息。

   ```js
   voiceManager.setCoord(23.025978, 113.754969)
   ```

5. 监听回调，可以监听识别状态、语义解析回调、TTS播报状态。

   ```js
   voiceManager.on(carvoiceassistant.EventType.VoiceAssistantEventTypeRecognizeStateChanged, (err, data) => {
         this.isRecognizing = data['isRecognizing']
         if (this.isRecognizing) {
           this.voiceText = "我正在听..."
         } else if (this.voiceText == "我正在听...") {
           this.voiceText = ''
         }
       })
       voiceManager.on(carvoiceassistant.EventType.VoiceAssistantEventTypeAsrResult, (err, data) => {
         let json: AsrModel = JSON.parse(data['result'])
         ...
       })
       voiceManager.on(carvoiceassistant.EventType.VoiceAssistantEventTypeTTSPlayStateChanged, (err, data) => {
         let isPlaying = data["isPlaying"]
         if (isPlaying == false) {
           if (this.needDeclare) {
             this.isUserStopRecognizing = false;
             this.needDeclare = false;
             voiceManager.startRecognize();
           }
           this.voiceText = '';
         }
       })
     }
   ```

   5. 识别接口

   ```js
   voiceManager.startRecognize(); //开始识别
   voiceManager.stopRecognize(); //停止识别
   ```

      

​      