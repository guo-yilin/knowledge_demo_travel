[博泰OpenHarmony语音助理](../../FA/PATEO_CarVoiceAssistant)
=========
### 概述

博泰OpenHarmony语音助理项目，是由博泰&开放原子基金会联合立项的项目，由博泰车联网（南京）有限公司负责研发。

该项目是基于OpenHarmony系统研发的一款语音类产品，包含了语音AI子系统和语音助理应用两大功能模块。

OpenHarmony语音助理突破了层层技术难关，实现了OH设备可见即可说的闭环能力，可对开发者基于语音能力的拓展开发进行赋能，对OpenHarmony系统生态共建具有里程碑的意义。

### UI效果图

![](./media/AppUI.jpg)

### 视频演示

[视频链接](https://www.bilibili.com/video/BV1Ed4y1t7SW/)

### 涉及OpenHarmony技术特性

- eTSUI

- 音频数据采集AudioStandard::AudioCapturer

- 音频播放OHOS::Media::Player

- 子系统开发

- IPC通信

- NAPI

### 基础信息

| 开发平台 | 系统类型 | 系统版本                | 开发语言     | IDE              |
| -------- | -------- | ----------------------- | ------------ | ---------------- |
| DAYU200  | 标准系统 | OpenHarmony 3.1 Release | C++、JS、eTS | VS code、Dev Eco |

**语音助理App源码目录：[源码目录](../../FA/PATEO_CarVoiceAssistant)**  

**语音AI子系统源码目录：[源码目录](../../dev/team_x/PATEO_CarVoiceAssistant)**  


### 软件架构
![框架图](media/架构图.jpg)

服务补充说明：

**QingAI：**博泰云服务，音频/文字互转，自然语言解析，最终语义反馈

**Sphinx：**唤醒词训练、声音采集，唤醒

**VAD：**语音活动检测，截取有效音频

**QGSpeechKit：**与QingAI云端通信的服务，含WebSocket网络连接，音频传输，云端ASR解析，NLU解析

**TTS****：**语音播报服务

**SA Service：**服务管理，跨进程通信服务

**NAPI：**服务层C++与应用层eTS转译服务

### 功能用例图

![用例图](media/功能图.png)

### 交互流程图

![流程图](media/流程图.jpg)



### 开发文档

本项目分为语音助理App和语音AI子系统两部分；

如果你只关注语音助理App部分，可以参考[语音AI子系统集成文档](./语音AI子系统集成文档.md)将语音子系统服务集成到开发板中，然后参考[语音助理App开发文档](./语音助理App开发文档.md)安装App体验。

如果你想深入了解语音子系统源码，可以参考[语音AI子系统开发文档](./语音AI子系统开发文档.md)。

