# [智能停车系统之门闸控制器](../../dev/team_x/GreyWolf_Latch)

## 一、设备展示

### 1. 样例效果

[智能停车系统之门闸控制器](../../dev/team_x/GreyWolf_Latch)基于OpenHarmony操作系统，设备控制使用欧智通BES2600开发板，系统版本为OpenHarmony3.1 Release，使用SG90舵机模拟门闸电机。手动按下开启门闸按键时，SG90舵机转动到打开角度、开发板蓝色LED灯点亮。当手动按下关闭门闸按键时，SG90舵机转动到闭合角度、开发板蓝色LED灯熄灭。

![index](media/latch_control.gif)

配合车辆出入检测、车牌识别设备、车辆管理终端设备可对停车场进行车辆出入管理的功能。设备初次上电时，红色LED灯以高频率闪烁，此时设备处于待配网状态。当使用配套手机FA配网后，红色LED灯以较低频率闪烁，此时设备处于连接到路由器状态。当红色LED灯常亮时，代表设备本地组网模块开始工作。

![records](media/latch_net_config.gif)



### 2. 涉及OpenHarmony技术特性

- HDF GPIO

### 3. 支持OpenHarmony版本

- OpenHarmony 3.1 Release

### 4. 支持开发板

- 欧智通BES2600开发板

## 二、快速上手

#### 1.轻量型设备环境准备

欧智通BES2600开发板：

- [BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/bes2600_hello_world)

#### 2.项目下载和导入

1）git下载

代码地址：[链接](../../dev/team_x/GreyWolf_Latch)

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_travel.git
git clone https://gitee.com/openharmony-sig/knowledge_demo_smart_home.git
```

2）项目导入

- 产品代码导入

  ```
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_Latch/door_access ~/OpenHarmony_3.1_Beta/vendor/team_x
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_Latch/common ~/OpenHarmony_3.1_Release/vendor/team_x
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_Latch/v200zr-evb-t1.hcs ~/OpenHarmony_3.1_Release/device/board/fnlink/shields/v200zr-evb-t1/
  sudo cp -rfa ~/knowledge_demo_travel/dev/team_x/GreyWolf_Latch/device_info.hcs ~/OpenHarmony_3.1_Release/device/soc/bestechnic/bes2600/liteos_m/components/hdf_config/
  ```

- 系统源码补充和修改

  - kernel/liteos_m 修改

    - 下载patch文件 [bes_kernle.patch](https://gitee.com/kenio_zhang/kernel_liteos_m/commit/d6e85103f49a12f13463dfd5b2bfd8274728d90a.diff)

      1.点击上述链接进入浏览器，将该网页中内容全部复制。

      2.本地创建一个名为bes_kernle.patch文件，并将已经复制的内容粘贴到该文件中。

    - 将patch文件打入源码

      1.将bes_kernle.patch文件复制到~/OpenHarmony_3.1_Beta/kernel/liteos_m目录下。

      2.执行如下命令将patch文件打入系统源码。

      ```
      cd ~/OpenHarmony_3.1_Beta/kernel/liteos_m
      patch -p1 < bes_kernle.patch
      ```

  - device/soc/bestechnic 修改

    - 下载patch文件 [bes_device.patch](https://gitee.com/kenio_zhang/device_soc_bestechnic/commit/55fbd420d5612773c47948d715ccddbff98379be.diff)。

      1.点击上述链接进入浏览器，将该网页中内容全部复制。

      2.本地创建一个名为bes_device.patch文件，并将已经复制的内容粘贴到该文件中。

    - 将patch文件打入源码

      1.将bes_kernle.patch文件复制到~/OpenHarmony_3.1_Beta/device/soc/bestechnic目录下。

      2.执行如下命令将patch文件打入系统源码。

      ```
      cd ~/OpenHarmony_3.1_Beta/device/soc/bestechnic
      patch -p1 < bes_device.patch
      ```

  - pwm_bes.c 修改

    - 参考如下diff文件修改drivers/adapter/platform/pwm/pwm_bes.c文件

      ```
      diff --git a/platform/pwm/pwm_bes.c b/platform/pwm/pwm_bes.c
      index 31348f8..3656373 100644
      --- a/platform/pwm/pwm_bes.c
      +++ b/platform/pwm/pwm_bes.c
      @@ -82,8 +82,29 @@ static int InitPwmDevice(struct PwmDev *host)
       }
      
       #ifdef LOSCFG_DRIVERS_HDF_CONFIG_MACRO
      +#define PLATFORM_PWM_CONFIG HCS_NODE(HCS_NODE(HCS_ROOT, platform), pwm0_config)
       static uint32_t GetPwmDeviceResource(struct PwmDevice *device)
       {
      +    uint32_t tempPin = 0;
      +    struct DeviceResourceIface *dri = NULL;
      +    struct PwmResource *resource = NULL;
      +    if (device == NULL) {
      +        HDF_LOGE("device is NULL\r\n");
      +        return HDF_ERR_INVALID_PARAM;
      +    }
      +
      +    resource = &device->resource;
      +    if (resource == NULL) {
      +        HDF_LOGE("resource is NULL\r\n");
      +        return HDF_ERR_INVALID_OBJECT;
      +    }
      +    tempPin = HCS_PROP(PLATFORM_PWM_CONFIG, pwmPin);
      +    resource->pwmPin = ((tempPin / DEC_TEN) * PIN_GROUP_NUM) + (tempPin % DEC_TEN);
      +    HDF_LOGI("pwmPin %d.\r\n", resource->pwmPin);
      +    resource->pwmId = HCS_PROP(PLATFORM_PWM_CONFIG, pwmId);
      +    HDF_LOGI("pwmId %d.\r\n", resource->pwmId);
      +
      +    return HDF_SUCCESS;
       }
       #else
       static uint32_t GetPwmDeviceResource(
      ```

    - 参考下方代码修改drivers\adapter\khdf\liteos_m\platform\BUILD.gn文件，将pwm_if.c纳入编译。

      ```
      diff --git a/khdf/liteos_m/platform/BUILD.gn b/khdf/liteos_m/platform/BUILD.gn
      index 84c9152..8a4016a 100755
      --- a/khdf/liteos_m/platform/BUILD.gn
      +++ b/khdf/liteos_m/platform/BUILD.gn
      @@ -63,7 +63,10 @@ hdf_driver("hdf_platform_lite") {
         }
      
         if (defined(LOSCFG_DRIVERS_HDF_PLATFORM_PWM)) {
      -    sources += [ "$HDF_FRAMEWORKS_PATH/support/platform/src/pwm/pwm_core.c" ]
      +    sources += [
      +      "$HDF_FRAMEWORKS_PATH/support/platform/src/pwm/pwm_core.c",
      +      "$HDF_FRAMEWORKS_PATH/support/platform/src/pwm/pwm_if.c",
      +    ]
         }
      
         if (defined(LOSCFG_DRIVERS_HDF_PLATFORM_RTC)) {
      ```

  - flash_size配置修改

    根据开发板的硬件版本不同，还需更改配置文件中flash_size的值。具体路径为/device/board/fnlink/v200zr/liteos_m/config.gni。其中v1.0版本对应16，2.0版本对应32。硬件版本打印在了开发板正面。

    ```
    diff --git a/v200zr/liteos_m/config.gni b/v200zr/liteos_m/config.gni
    index b288101..6489dd2 100644
    --- a/v200zr/liteos_m/config.gni
    +++ b/v200zr/liteos_m/config.gni
    @@ -47,7 +47,7 @@ if (product_path != "") {
       bsp_bin_list = product_conf.bsp_bin_list
       pack_burn = product_conf.pack_burn
     }
    -flash_size = 16
    +flash_size = 32
    ```

#### 3.编译和烧录

参考[BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/bes2600_hello_world)完成项目的编译和烧录。

## 三、关键代码解读

#### 1.目录结构

```
.
├─vendor
   └─team_x
       └─door_access
           ├─demo_door_access
           │  ├─external_device //基于HDF的外设驱动
           │  │   ├─key.c // 按键
           |  |   ├─led.c // LED灯
           |  |   └─sg90.c // sg90舵机
           │  ├─local_net // 本地组网通信
           │  │   ├─local_net_communication.c // 本地组网通信协议
           │  │   ├─local_net_udp.c // 本地组网udp通信
           │  │   ├─local_net_dlist.c // 链表存储
           │  │   └─local_net_utils.c // 工具类接口（SHA256等）
           │  ├─iot_main.c// 门闸控制主逻辑
           └─config.json等配置文件及文件夹 设备通用
```

#### 2.关键代码

创建门闸控制任务和LocalNet本地网络通信启动任务。

![](media/iot_main_start_code.png)

门闸控制任务中，按键、LED指示灯、SG90初始化后进入任务循环。循环中读取按键状态。

![](media/latch_control_task_init.png)

根据按键状态，设置门闸开启、关闭、以及路由信息的删除。

![](media/latch_control_task_local_control.png)

当门闸状态发生变化时，且LocalNet本地网络通信模块初始化完成时，发布门闸状态。

![](media/latch_control_task_localnet_send.png)

在本地通信模块启动任务中，首先读取路由信息，当未读取到路由信息时，启动softap配网并设置红色LED灯快速闪烁指示当前状态，否则跳过该步骤。网络配置完成或路由信息读取成功后，连接路由、重新写入路由信息、设置红色LED灯较慢速率闪烁指示当前状态。

![](media/local_net_task_config_and_connect_wifi.png)

配置设备自身信息，并对LocalNet本地通信息模块进行初始化、设置红色LED灯常亮指示当前状态。

![](media/local_net_self_info_set_and_init.png)

## 四、样例联动

本样例可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)

## 五、参考链接

- [BES2600快速上手指导文档](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/bes2600_hello_world)