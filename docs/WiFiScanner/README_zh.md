# WIFI扫描仪

## 应用端

### 简介

&ensp;&ensp;&ensp;&ensp;WiFi扫描仪是装在手机端的应用，适用于OpenHarmony3.1_release版本，使用wifi提供的接口实现WiFi的扫描，连接以及获取连接AP的信息等功能。</br>
&ensp;&ensp;&ensp;&ensp;样例目前在KHDVK-3566B设备上进行布局适配
### 样例效果
![](res/1.jpg)

![](res/2.jpg)

![](res/3.jpg)

&ensp;&ensp;&ensp;&ensp;本样例包含WiFi的扫描和连接，扫描的结果通过列表的方式展现，可以通过点击扫描的结果来进行WiFi热点的连接。
首页中会显示已连接WiFi热点的一些信息。

###代码目录
![](res/4.png)


### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_travel/tree/master/FA/WiFiScanner)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta3 。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程WiFiScanner。

4）OpenHarmony应用运行在真机设备上，需要进行签名，[签名方法](https://docs.openharmony.cn/pages/v3.1/zh-cn/application-dev/security/hapsigntool-guidelines.md/)。
