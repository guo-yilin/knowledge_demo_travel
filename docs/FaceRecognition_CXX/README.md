# SeetaFace2 人脸识别样例

## 样例简介

本样例是在搭载了OpenHarmony3.2beta1的系统的RK3568开发板上运行，实现了通过输入一张人脸图，通过SeetaFace2进行识别，最终输出识别图片的名字。后续将提供可以通过从摄像头进行图片获取进行识别功能。

### 样例原理

本样例通过输入一张人脸图进行识别，样例默认注册注了几个人脸图。识别的主要步骤：

- 人脸检测，在图像中首先定位人脸位置。
- 通过人脸位置进行面部关键点定位。
- 关键点定位后提取对应的面部特征信息。
- 提取到的面部特征信息与已注册的人脸面部信息进行对比，通过计算人脸的相似度完成人脸识别功能。

### 工程版本

- 系统版本：OpenHarmony 3.2 beta1
- hb版本：0.4.6

## 快速上手

### 准备硬件环境

- 预装windows系统的PC机
- RK3568开发板一个

### 准备开发环境

本demo是基于OpenHarmony-v3.2-Beta1版本，在RK3568开发板上验证的，对开发环境的准备，可以先参考[润和RK3568开发板标准系统快速上手](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld#%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA)。

### 准备工程

本用例采用repo的方式从码云官仓下载系统系统源码以及开发板适配代码，使用git从gitee的sig仓库拉取设备应用代码。

#### 配置git

- 提前注册准备码云gitee账号。
- git工具下载安装

  ```shell
  sudo apt install git
  sudo apt install git-lfs
  ```

- 生成/添加SSH密钥：生成密钥 使用gitee账号绑定的邮箱生成密钥对

  ```shell
  ssh-keygen -t ed25519 -C "xxxxx@xxxxx.com"
  ```

- 查看生成的密钥

  ``` shell
  cat ~/.ssh/id_ed25519.pub
  ```

- 复制生成后的 ssh key，返回gitee个人主页，通过主页 「个人设置」->「安全设置」->「SSH 公钥」 ，将生成的“SSH密钥”添加到仓库中。
- 配置git用户信息
  ``` shell
  git config --global user.name "yourname"
  git config --global user.email "your-email-address"
  git config --global credential.helper store
  ```

#### 准备repo

  ``` shell
  curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  ## 如果没有权限可以，可先将repo下载到当前目录在拷贝
  chmod a+x /usr/local/bin/repo
  pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
  ```
#### 准备系统源码

本样例是基于OpenHarmony3.2 Beta版本开发的，所以需要下载OpenHarmony3.2 Beta版。

```shell
mkdir ~/OpenHarmony3.2
cd ~/OpenHarmony3.2
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.2-Beta1 --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 准备开发板适配代码

具体仓库地址: [knowledge_demo_travel](https://gitee.com/openharmony-sig/knowledge_demo_travel)  <br>
通过git命令下载并拷贝道：OpenHarmony对应目录

```shell
cd ~
git clone https://gitee.com/openharmony-sig/knowledge_demo_travel.git --depth=1
cp -raf knowledge_demo_travel/dev/teamx/SeetaFaceDemo ~/OpenHarmony3.2/
```

#### 代码修改

- 添加子系统    <br>
  打开//build/subsystem_config.json文件，在文件末尾添加以下代码:

  ```json
  "SeetaFaceDemo": {
    "path": "SeetaFaceDemo",
    "name": "demo_app"
  }
  ```

- 将该子系统添加到编译构建中    <br> 
  打开文件//vendor/hihope/rk3568/config.json, 在thirdparty子系统后面添加SeetaFaceDemo子系统：

  ``` json
  {
    "subsystem":"SeetaFaceDemo",
    "components":[
      {
        "component":"SeetaFace_demo",
        "features":[]
      }
    ]
  }
  ```
### 编译

进入到OpenHarmony系统源码根目录下，输入hb set并选择rk3568

```shell
hb set          ## 产品列表选择，此处选择rk3568
hb build -f     ## 开始全量编译。(hb build 为增量编译)
```

编译成功后会在//out/rk3568/SeetaFaceDemo/SeetaFace/目录下生成seetaface_example.

### 烧入/安装

编译成功后可以将seetaface_example程序和测试图片，模型文件(模型文件在[SeetaFace2 github](https://github.com/seetafaceengine/SeetaFace2)上可下载)一起打包，通过hdc_std工具将文件推送到RK3568的开发板中，具体命令:

```shell
hdc_std file send .\seetaface.tar /data/        ## 将打包文件seetaface.tar推送到开发板的data目录
```

### 运行应用

将应用与图片资源,模型文件夹放在同一目录，直接执行 

```shell
./seetaface_example ./1.jpg,
```

当图片1.jpg中的人脸已经预置时，串口输出 `get result! name:xxx`,否则输出`ignored`。

## 参考资料

- [人脸识别库的移植](https://gitee.com/openharmony-sig/knowledge_demo_travel/blob/master/docs/FaceRecognition_NAPI/%E4%BA%BA%E8%84%B8%E8%AF%86%E5%88%AB%E5%BA%93%E7%9A%84%E7%A7%BB%E6%A4%8D.md)
- [OpenHarmony 出行场景样例](https://gitee.com/openharmony-sig/knowledge_demo_travel)
- [OpenHarmony 知识体系](https://gitee.com/openharmony-sig/knowledge)
