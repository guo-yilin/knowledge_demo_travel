 数据爬虫
===========
**简要介绍**<br>

>考虑到我们的项目最终需要为用户提供景点规划，关于全国景点数据的爬取便成为我们项目实现的最基本的一步。
>
>在数据来源的选择上，对比携程、去哪儿网、马蜂窝等旅游网站，我们最终选择了去哪儿网，它能够通过用户输入的城市关键词整理出该城市大量景点信息，包括景点详细地址、景点简介、配图、景点经纬度等，不仅如此，还有景点评分、排名等能帮助我们最终规划的有效数据。
>
>本次爬虫利用python进行设计，python自带的request、BS4等库为爬虫的实现提供了极大便利。具体实现将在流程框架中具体展示。

# 流程框架

![reptailFrame](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/reptailFrame.png)

# 关键解读

## 1、城市id的爬取

在去哪儿网站中，为了方便查找和整理数据，对于每一个城市或景点都对应着一个id。一般来说，城市的id格式为“csxxxxxx”（cs后面为六位数字编码），如北京id为“cs299914”。因此，我们如果想要爬取对应城市的景点信息，首先需要爬取得到各个城市的id。
在经过多次查找后，我们发现在[这个页面](https://travel.qunar.com/place/)中包含有城市id的数据（如图所示），因此我们编写了citycode.py文件来爬取城市对应的id（代码如下）。

![idofcity](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/idofcity.png)

```python
from bs4 import BeautifulSoup
import pandas as pd
import requests

def get_static_url_content(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}
    req=requests.get(url,headers=headers)
    content=req.text
    bsObj=BeautifulSoup(content,'lxml')
    return bsObj

def get_city_id():
    url = 'http://travel.qunar.com/place/'
    bsObj=get_static_url_content(url)
    cat_url = []
    cat_name = []
    code={}
    bs=bsObj.find_all('div',attrs={'class':'sub_list'})

    for i in range(0,len(bs)):
        xxx = bs[i].find_all('a')
        for j in range(0,len(xxx)):
            # cat_name.append(xxx[j].text)
            name=xxx[j].text
            cat_name.append(name)
            # cat_url.append(xxx[j].attrs['href'])
            id=xxx[j].attrs['href']
            code[name]=id
    return cat_name,code

# city_name_list,city_url_list=get_city_id()
# city=pd.DataFrame({'city_name':city_name_list,'city_code':city_url_list})
# city.to_csv('city.csv',encoding='utf_8_sig')
# namelist,code=get_city_id()
```

最终可以爬取出382个城市的id，存入csv文件中（如图所示，展示部分id）。

![citys](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/citys.png)

## 2、景点信息的爬取

获取到了城市的id，就可以根据url的结构构造出每个城市所对应的景点信息页面的url。这里我们编写了citydata.py文件来实现对景点信息的爬取，爬取到的信息有景点名称、景点简介、景点图片、景点评分等等，最终存入“去哪儿网数据爬取.xlsx”文件中（如图所示，展示部分信息）。
值得一提的是，在设计爬虫文件时，由于测试数据的频繁操作，可能影响到了原网站的服务维护，因此设计的第一版爬虫方案由于原网页的反爬机制最终以失败告终，这也导致我们不得不重新查找新的包含景点信息数据的网页。不过还好，最终我们找到了[这个页面](http://travel.qunar.com/p-cs299914-beijing-jingdian)，此外在这一次编写时，我们吸取了上次失败的经验，采用了随机使用ua伪装成浏览器和添加随机“休息时间”伪装成用户的方法，成功应对了去哪儿网针对爬虫的反爬机制，最终总算是可以提取出景点信息。但是由于休息时间的设置，导致爬取时间大幅增加，不过这也是目前我们已知的最优解决方案。

![cityres](https://gitee.com/muqinga/online_event_1/raw/master/college_growth_program/growth_project/%E9%AB%98%E9%9B%85%E9%9B%AF%20%E6%97%85%E8%A1%8C%E5%B0%8F%E5%8A%A9%E6%89%8B/%E6%BC%94%E7%A4%BA%E4%B8%8E%E8%AF%B4%E6%98%8E/%E5%AA%92%E4%BD%93%E8%B5%84%E6%BA%90/cityres.png)

代码如下：

```python
import requests
from bs4 import BeautifulSoup
import pandas as pd
from fake_useragent import UserAgent
from time import sleep
import random
import citycode


def get_url(key,n):
	'''
	【分页网址url采集】函数
	n：页数参数
	结果：得到一个分页网页的list
	'''
	lst = []
	for i in range(n):
		ui = str(key)+"-jingdian-1-{}".format(i + 1)
		lst.append(ui)
	return lst


def get_data(ui, d_h, d_c, keyword):
	'''
	【数据采集】
	ui：数据信息网页
	d_h：user-agent信息
	d_c：cookies信息
	结果：得到数据的list，每条数据用dict存储
	'''
	ri = requests.get(ui, headers=dic_heders, cookies=dic_cookies)
	sleep(random.uniform(1, 2))
	soup_i = BeautifulSoup(ri.text, 'lxml')
	ul = soup_i.find("ul", class_="list_item clrfix")
	lis = ul.find_all('li')

	lst = []
	for li in lis:
		dic = {}
		dic['景点名称'] = li.find('span', class_="cn_tit").text
		dic['景点关键词'] = keyword
		dic['景点图片'] = li.find('a',class_="imglink").img['src']
		dic['攻略数量'] = li.find('div', class_="strategy_sum").text
		dic['评分'] = li.find('span', class_="total_star").span['style']
		dic['简介'] = li.find('div', class_="desbox").text
		dic['排名'] = li.find('span', class_="ranking_sum").text
		dic['经度'] = li['data-lng']
		dic['纬度'] = li['data-lat']
		dic['点评数量'] = li.find('div', class_="comment_sum").text
		dic['多少驴友来过'] = li.find('span', class_="comment_sum").span.text
		lst.append(dic)
	return lst


if __name__ == "__main__":

	# dic_heders = {
	# 	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
	# }

	dic_cookies = {}
	cookies = 'QN1=dXrgj14+tmYQhFxKE9ekAg==; QN205=organic; QN277=organic; QN269=506F28C14A7611EAA0BEFA163E244083; _i=RBTKSRDqFhTQT5KRlx-P1H78agxx; fid=7cc3c3d9-3f6c-45e1-8cef-3384cd5da577; Hm_lvt_c56a2b5278263aa647778d304009eafc=1581168271,1581220912; viewpoi=7564992|709275; viewdist=299878-7; uld=1-299878-8-1581221233|1-1062172-1-1581168529; QN267=1679639433d5aedfc8; Hm_lpvt_c56a2b5278263aa647778d304009eafc=1581221236; QN25=cb06bfbd-d687-4072-98c5-73266b637a6a-9f992f90; QN42=nvxp8441; _q=U.qunar_lbs_428305502; _t=26463150; csrfToken=oXYBnhSoGAGxggRkzmAjbxxGrpgsjUqQ; _s=s_ZBWFJO3EEGZISWS35EBIS5NQYA; _v=YTRjW_H5L47nGNVabvTLt1mlh7j8R7t4UNDVRrJUz0wScfLMWgSvkwQbzMLHlFbsvTU-2kJrBK74NUyOi3MX_3obY94Hhhugt8bv8ILxwsWDv4s_ANNiM8qRdg6HlBrrCEnGYr8lxS9uv78zDCNKz9pFbN8JPYy-AKJP6xILIsT7; _vi=4ONQzvfOOhwJECN5R-4rfWZDzlQ5-qv2xi_jsp1INPEpy9iKHa5gV0gHc35fDfTDe3TjcKteU7ZWk1vd6MsIqTfXYyUh3gTwZJ_9z3PEpkXZReeeIjaVE4HwLTkOATLIzIxg92s-QCWKE1RdNlaZsxPnfN7NHPGAZz5rsmxvpNDY; QN44=qunar_lbs_428305502; QN48=tc_a7fe4861b2d918df_17028369fc8_67ab; QN271=1749d44a-1a11-4886-be27-c3e3bfdadb0c'
	cookies_lst = cookies.split("; ")
	for i in cookies_lst:
		dic_cookies[i.split("=")[0]] = i.split("=")[1]

	citylist,code=citycode.get_city_id()
	citylist_=['北京','上海','重庆','天津','厦门','福州','泉州','南平','宁德','杭州','宁波','台州','湖州','南京','苏州','扬州','无锡','连云港','南通','青岛','济南','合肥','黄山','大连','沈阳','石家庄','哈尔滨','太原','呼和浩特','呼伦贝尔','长春','三亚','海口','广州','深圳','珠海','长沙','武汉','宜昌','成都','昆明','大理','宝鸡','拉萨','兰州','乌鲁木齐','遵义','银川']
	# citylist_=['北京']

	datalst = []
	errorlst = []
	for item in citylist_:
		key=code[item]
		for u in get_url(key,15):
			try:
				ua = UserAgent(verify_ssl=False)
				dic_heders = {"User-Agent": ua.random}
				datalst.extend(get_data(u, dic_heders, dic_cookies,item))
				print('数据采集成功，共采集数据{}条'.format(len(datalst)))
			except:
				errorlst.append(u)
				print('数据采集失败，网址为：', u)

	df = pd.DataFrame(datalst)
	df['经度'] = df['经度'].astype('float')
	df['纬度'] = df['纬度'].astype('float')
	df['点评数量'] = df['点评数量'].astype('int')
	df['攻略数量'] = df['攻略数量'].astype('int')
	df['评分'] = df['评分'].str.split(":").str[-1].str.replace("%", "").astype("float")
	df['多少驴友来过'] = df['多少驴友来过'].str.replace("%", "").astype('float') / 100
	df['排名'] = df[df['排名'] != ""]['排名'].str.split("第").str[-1].astype('int')

	df.to_excel('去哪儿网数据爬取.xlsx', index=True)
```

