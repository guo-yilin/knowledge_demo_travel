## 从零开发停车场管理系统

### 简介

车辆管理中控台应用基于OpenHarmony 3.1 Release，使用eTS编写，可实现车辆出入信息管理、手动开门禁、显示在线设备列表和收费管理等功能。

### 工程目录

完整的项目结构目录如下

```shell
.
├─entry
│  └─src
│      └─main
│          ├─ets
│          │  └─MainAbility
│          │      ├─common // 工具类
│          │      │   ├─CommonLog.ets // 日志
│          │      │   ├─StorageUtils.ets // 本地存储
│          │      │   ├─TabsNavigation.ets // 自定义Tab导航栏
│          │      └─pages // 页面文件
│          │          ├─index.ets // 首页
│          │          ├─index.ets // 门禁记录页
│          └─resources // 资源文件
└─signature // 签名文件
```

### 开发步骤

#### 1. 新建OpenHarmony ETS项目

在DevEco Studio中点击File -> New Project ->[Standard]Empty Ability->Next，Language 选择ETS语言，最后点击Finish即创建成功。
![image-20211124092813545](media/new_project.png)

#### 2. 编写页面

以首页为例，页面也分为如下3部分：顶部标题栏、自定义tab顶部导航栏、TabContent内容区。

![index](media/index_layout.png)

##### 2.1 顶部标题栏

1）首先使用flex布局做为容器，并使用justifyContent: FlexAlign.SpaceBetween属性，使其两端对齐；

2）容器中写入相应组件，并且左侧使用Text组件进行占位，从而做到上图中的效果；

3）门禁记录文本设置点击事件；

```js
Flex({
    direction: FlexDirection.Row,
    alignItems: ItemAlign.Center,
    justifyContent: FlexAlign.SpaceBetween
}) {
    Text().width(100)
    Text('停车场管理系统').fontColor(Color.White).fontSize(32)
    Text('门禁记录').fontColor('#4E98FF').fontSize(24).width(100)
}.margin({ left: 16, right: 16 }).onClick(()=>{
    router.push({uri:'pages/accessRecords'})
}).height(130)
```

##### 2.2 自定义tab顶部导航栏

由于Tabs组件中顶部导航栏中可自定义的样式比较少，所以自定义一个tab顶部导航栏实现页签切换，因此该组件除了可实现不同样式外，还能实现tab页签切换功能。

1）确定传入的参数：当前tab索引值、TabsController用于控制Tab内容区切换内容、groups用于显示页签值。

```js
@Link tabIndex: number // tab索引值
private controller: TabsController = new TabsController()
private groups: Array<string> = [] // 分组 用于显示页签
```

2）遍历group生成Text组件

```js
Flex({alignContent:FlexAlign.Center}) {
    Row() {
        ForEach(this.groups, (item: string,index:number) => {
            Text(item.toString() + '门')
                .width(130)
                .fontSize(index === this.tabIndex ? 36 : 32)
                .fontWeight(index === this.tabIndex ? 600 : 500)
                .textAlign(TextAlign.Center)
                .fontColor(index === this.tabIndex ? '#FFFFFF' : '#99FFFFFF')
                .onClick(() => {
                    this.tabIndex = index
                    // 控制内容区进行切换
                    this.controller.changeIndex(this.tabIndex)
                })
        }, item => item)
    }.margin({bottom:30})
    .constraintSize({ minWidth: '100%' })
    .backgroundColor('#00ffffff')
}
```

3）首页导入组件并使用

```js
// 导入导航栏
import TabsNavigation from '../common/TabsNavigation.ets'

TabsNavigation({ tabIndex: $tabIndex, groups: this.groups, controller: this.controller })
```

##### 2.3 TabContent内容区

1）编写EntranceCard组件，即入口/出口部分组件；

2）编写Tab组件，由于顶部导航栏使用自定义组件，所有需设置barHeight高度为0

```js
// 设置页面位于顶部
Tabs({ barPosition: BarPosition.Start, index: 0, controller: this.controller }) {
    TabContent() {
        EntranceCard({ group:'A_ENTRANCE', tabDeviceList: $groupAEntrance })
        EntranceCard({ group:'A_EXIT',tabDeviceList: $groupAExit })
    }.tabBar('A').margin({ left: 16, right: 16 })

    TabContent() {
        EntranceCard({ group:'B_ENTRANCE',tabDeviceList: $groupBEntrance })
        EntranceCard({ group:'B_EXIT', tabDeviceList: $groupBExit })
    }.tabBar('B').margin({ left: 16, right: 16 })

    TabContent() {
        EntranceCard({ group:'C_ENTRANCE',tabDeviceList: $groupCEntrance })
        EntranceCard({ group:'C_EXIT', tabDeviceList: $groupCExit })
    }.tabBar('C').margin({ left: 16, right: 16 })

    TabContent() {
        EntranceCard({ group:'D_ENTRANCE',tabDeviceList: $groupDEntrance })
        EntranceCard({ group:'D_EXIT',tabDeviceList: $groupDExit })
    }.tabBar('D').margin({ left: 16, right: 16 })
}
.vertical(false)
.scrollable(true)
.barMode(BarMode.Fixed)
.barWidth(400)
.barHeight(0) // 顶部导航栏高度
.animationDuration(400)
.colorBlend(Color.White)
.onChange((index: number) => {
    this.tabIndex = index
})
```

#### 3. 停车场NAPI调用

NAPI（Native API）组件是一套对外接口基于Node.js N-API规范开发的原生模块扩展开发框架。我们将需要与其他设备进行联动的接口通过NAPI封装至ParkingApi中，其中包含的接口参照下图。

![napi.png](media/napi.png)

1）调用init初始化ParkingApi使当前设备上线

2）初始化成功后，获取当前设备列表，并对监听是否有车辆靠近和设备列表是否更新

```js
aboutToAppear() {
    CommonLog.info(TAG,"aboutToAppear")
    featureAbility.getContext().getOrCreateLocalDir((err, data) => {
        CommonLog.info(TAG,"create log file success" + data)
        data += "/files"
        // 初始化
        ParkingApi.init(data).then((result) => {
            CommonLog.info(TAG,`ParkingApi init success :${JSON.stringify(result)}`)
            // 获取设备列表
            ParkingApi.getDeviceList().then(res=>{
                CommonLog.info(TAG,`aboutToAppear getDeviceList = ${JSON.stringify(res)}`)
                this.deviceList = res
            })
            // 监听车辆靠近事件
            this.setListenCarAccess()
            // 监听设备更新事件
            ParkingApi.on('devicesUpdate',(res)=>{
                CommonLog.info(TAG,`devicesUpdate res = ${JSON.stringify(res)}`)
                this.resetDeviceData();
                this.deviceList = res.devicesUpdate
                this.initTabDeviceList()
            })
        });
    })

    this.tabIndex = 0
}
```

3）编写车辆来临或者离开时，门禁逻辑

```js
setListenCarAccess(){
    //注册carAccess
    CommonLog.info(TAG,`========setListenCarAccess==========`)
    ParkingApi.on('carAccess',(licensePlate,group,state)=>{
        CommonLog.info(TAG,`========setListenCarAccess callback start==========`)
        CommonLog.info(TAG,`========setListenCarAccess callback arguments:${JSON.stringify(arguments)}`)
        if(state == 'waiting'){
            // 保存车牌并开门
            this.saveLicensePlate(licensePlate,group)
        }else if(state == 'leave'){
            // 控制关门
            controlLatch(group,'close')
        }
        // 保存当前组车辆出入数量
        StorageUtils.saveCardCount(group)
    }).catch(err=>{
        CommonLog.error(TAG,`setListenCarAccess error:${JSON.stringify(err)}`)
    })
}
```

4）在应用退出时，释放资源并让设备下线

```js
onDestroy() {
    CommonLog.info(TAG,'entry Application onDestroy')
    // 释放资源
    let result = ParkingApi.release()
    CommonLog.info(TAG,`ParkingApi release success :${JSON.stringify(result)}`)
    CommonLog.info(TAG,'Application onDestroy')
}
```

### 项目下载和导入

项目仓库地址：https://gitee.com/openharmony-sig/knowledge_demo_temp/

1）git下载

```shell
git clone https://gitee.com/openharmony-sig/knowledge_demo_temp.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Entertainment/BombGame

### 约束与限制

#### 1. 设备编译约束

润和RK3568开发板：

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)

#### 2. 应用编译约束

- 参考 [应用开发快速入门](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/Readme-CN.md)
- 集成开发环境：DevEco Studio 3.0.0.800版本,[下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- OpenHarmony SDK 3.0.0.0；
