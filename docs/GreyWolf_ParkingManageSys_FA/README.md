# [智能停车系统之车辆管理中控台](../../FA/GreyWolf_ParkingManageSys_FA)

## 简介

智能停车系统之车辆管理中控台是由本FA应用和[车辆管理中控台NAPI](../GreyWolf_ParkingManageSys_Dev/README.md)组成。

### 样例效果

[车辆管理中控台应用](../../FA/GreyWolf_ParkingManageSys_FA)基于OpenHarmony 3.1 Release，使用eTS编写，配合对应NAPI可实现车辆出入信息管理、手动开门禁、显示在线设备列表和收费管理等功能。

![index](media/index.png)

![records](media/records.png)

### 涉及OpenHarmony技术特性

- eTS声明式开发范式
- NAPI

### 支持OpenHarmony版本

- OpenHarmony 3.1 Release

### 支持开发板

- 润和RK3568开发板

## 快速上手

### 标准设备环境准备

润和RK3568开发板：

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld);

### 应用编译环境准备

- 下载DevEco Studio for OpenHarmony [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);
- DevEco Studio 点击File -> Open 导入本下面的代码工程ParkingSystem;

### 项目下载和导入

1）git下载

应用代码：[链接](../../FA/GreyWolf_ParkingManageSys_FA)

```shell
git clone https://gitee.com/openharmony-sig/knowledge_demo_travel.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/knowledge_demo_travel/FA/GreyWolf_ParkingManageSys_FA

3）NAPI声明文件导入

声明文件定义了所有NAPI的接口函数以及对应的入参和返回值，可方便IDE进行检查和识别，也可快速让开发人员了解到所定义的接口，所以开发前我们需要将 [@ohos.ParkingApi.d.ts](media/@ohos.ParkingApi.d.ts)  放入 OpenHarmony SDK目录/ets/当前使用版本/api/common 目前下，其中OpenHarmony SDK目录可通过DevEco->File->SDK Manager->OpenHarmony SDK进行查看。

### 安装应用

1）将设备连接到电脑，并确定DevEco Studio识别到设备

2）点击Run->Run进行安装

## 关键代码解读

### 目录结构

```shell
.
├─entry
│  └─src
│      └─main
│          ├─ets
│          │  └─MainAbility
│          │      ├─common // 工具类
│          │      │   ├─CommonLog.ets // 日志
│          │      │   ├─StorageUtils.ets // 本地存储
│          │      │   ├─TabsNavigation.ets // 自定义Tab导航栏
│          │      └─pages // 页面文件
│          │          ├─index.ets // 首页
│          │          ├─index.ets // 门禁记录页
│          └─resources // 资源文件
└─signature // 签名文件
```

### 日志查看

```shell
hdc_std shell
hilog | grep ParkingSystem 
```

### 关键代码

关键代码参考 [从零开发停车场管理系统](quick_develop.md) NAPI调用章节

## 样例联动

本样例由苍狼战队开发，可与其他智能停车场样例组合产品联动效果，参考 [智能停车场](../GreyWolf_SmartParking/README.md)

## 参考链接

- [智能场景车辆管理中控台NAPI接口](../GreyWolf_ParkingManageSys_Dev/README.md)
- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)
